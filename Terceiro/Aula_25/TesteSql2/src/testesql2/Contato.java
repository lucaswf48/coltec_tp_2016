package testesql2;

public class Contato {
    
    private int id;
    private String nome;
    private String lastName;
    private String email;
    private String endereco;
    private String dataNascimento;
    private LocalDeTrabalho localDeTrabalho;
    private int localDeTrabalhoId;


    public int getId() {
        return id;
    }


    public void setId(int id) {
        this.id = id;
    }


    public String getNome() {
        return nome;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getEmail() {
        return email;
    }


    public void setEmail(String email) {
        this.email = email;
    }


    public String getEndereco() {
        return endereco;
    }


    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }


    public String getDataNascimento() {
        return dataNascimento;
    }
 
    public void setDataNascimento(String data){
        this.dataNascimento = data;
    }

    public void setLastName(String last) {
       
        this.lastName = last;
    }
    
    public String getLastName(){
        
        return this.lastName;
    }


    public LocalDeTrabalho getLocalDeTrabalho() {
        return localDeTrabalho;
    }


    public void setLocalDeTrabalho(LocalDeTrabalho localDeTrabalho) {
        this.localDeTrabalho = localDeTrabalho;
    }


    public int getLocalDeTrabalhoId() {
        return localDeTrabalhoId;
    }

    public void setLocalDeTrabalhoId(int localDeTrabalhoId) {
        this.localDeTrabalhoId = localDeTrabalhoId;
    }
}

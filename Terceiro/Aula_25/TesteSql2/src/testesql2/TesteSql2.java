package testesql2;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class TesteSql2 {

    /**
     * @param args the command line arguments
     * @throws java.sql.SQLException
     */
    public static void main(String[] args) throws SQLException {
        
        ConnectionFactory conect = new ConnectionFactory();

        LocalDeTrabalhoDAO localdao = new LocalDeTrabalhoDAO();
        LocalDeTrabalho local = new LocalDeTrabalho();
        
        local.setEndereco("Antonio carlos");
        local.setNome("ufmg");
        local.setId(1);
        localdao.insere(local);
        
        //localdao.remove(local);
        List<LocalDeTrabalho> daw = localdao.localiza("coltec");

        for(LocalDeTrabalho l : daw){
            System.out.println(l.getEndereco());
            System.out.println(l.getNome());
        }
    }
    

    
}

package testesql2;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class ContatoDAO {
    
    private Connection connection;
    
    public ContatoDAO(){
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Contato contato){
        
        String insert = "insert into contatos " + "(nome,email,endereco,dataNascimento,last_name,local_de_trabalho_id)" + " values (?,?,?,?,?,?)";
        
        try{
            PreparedStatement stmt = this.connection.prepareStatement(insert);
            stmt.setString(1,contato.getNome());
            stmt.setString(2,contato.getEmail());
            stmt.setString(3,contato.getEndereco());
            stmt.setString(4,contato.getDataNascimento());
            stmt.setString(5,contato.getLastName());
            stmt.setInt(6,contato.getLocalDeTrabalhoId());
            stmt.execute();
            stmt.close();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
    
    public List<Contato> selec() throws SQLException{
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos");
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    public List<Contato> localizaContatoPrimeiroNome(String nome) throws SQLException{
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos where nome like ?");
        
        
        stmt.setString(1, nome);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    public List<Contato> localizaContatoLastName(String nome) throws SQLException{
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos where last_name like ?");
        
        
        stmt.setString(1, nome);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    
    public void alteraContato(Contato contato)throws SQLException{
        PreparedStatement stmt = connection.prepareStatement("update contatos set nome = ? , email = ? , endereco = ?,dataNascimento = ? ,last_name = ? where id = ?");
        
        stmt.setString(1,contato.getNome());
        stmt.setString(2,contato.getEmail());
        stmt.setString(3,contato.getEndereco());
        stmt.setString(4,contato.getDataNascimento());
        stmt.setString(5,contato.getLastName());
        stmt.setInt(6, contato.getId());
        
        stmt.execute();
        stmt.close();
    }
    
    public void removeContato(Contato contato) throws SQLException{
        
        PreparedStatement stmt = connection.prepareStatement("delete from contatos where id = ?");
        
        stmt.setInt(1,contato.getId());
        stmt.execute();
        stmt.close();
    }
    
    
}

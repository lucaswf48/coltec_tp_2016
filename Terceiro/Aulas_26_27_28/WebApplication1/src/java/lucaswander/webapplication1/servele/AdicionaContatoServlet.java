/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.webapplication1.servele;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.webapplication1.jdbc.*;

/**
 *
 * @author strudel
 */

@WebServlet(
        name = "adicionaContato",
        urlPatterns = "/adicionacontato"
)
public class AdicionaContatoServlet extends HttpServlet{
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
        

        try {
            PrintWriter out = response.getWriter();
            
            String nome = request.getParameter("nome");
            String email = request.getParameter("email");
            String endereco = request.getParameter("endereco");
            String dataNascimento = request.getParameter("dataNascimento");
            String lastName = request.getParameter("lastName");
            
            Contato contato = new Contato();
            
            contato.setDataNascimento(dataNascimento);
            contato.setEmail(email);
            contato.setEndereco(endereco);
            contato.setNome(nome);
            contato.setLastName(lastName);
            contato.setLocalDeTrabalhoId(1);
            
            ContatoDAO dao = new ContatoDAO();
            dao.adiciona(contato);
            
            RequestDispatcher rd = request.getRequestDispatcher("/contato-adicionado.jsp");
            rd.forward(request, response);
                    
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(AdicionaContatoServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            throw new  ServletException(ex);
        }
    }
}

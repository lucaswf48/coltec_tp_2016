/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.webapplication1.servele;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author strudel
 */
@WebServlet(
        name = "minha", 
        urlPatterns = {"/oi", "/ola"},
        initParams = {
            @WebInitParam(name = "param1", value = "value1"),
            @WebInitParam(name = "param2", value = "value2")}
)
public class OiMundo extends HttpServlet{
    
    
    private String parametro1;
    private String parametro2;
    
    public void init(ServletConfig config) throws ServletException{
        
        super.init(config);
        this.parametro1 = config.getInitParameter("param1");
        this.parametro2 = config.getInitParameter("param2");
        
    }
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        
//        PrintWriter out = response.getWriter();
//        
//        out.println("<html>");
//        out.println("<body>");
//        out.println("Primeira Servlet");
//        out.println("</body>");
//        out.println("</html>");
        
//        response.setContentType("text/html");
//        PrintWriter out = response.getWriter();
//        
//        out.println("<h2>Exemplo com InitParam Servlet</h2>");
//        ServletConfig config = getServletConfig();
//        
//        String para1 = config.getInitParameter("param1");
//        out.println("Valodr od parâmetro 1: " + para1);
//        
//        String para2 = config.getInitParameter("param2");
//        out.println("<br>Valor do parâmetro 2:" + para2);
        
//        PrintWriter out = response.getWriter();  
//        out.println("Valor do parâmetro 1:" + getServletConfig().getInitParameter("param1"));
        
        
        String nome = request.getParameter("nome");
        String email = request.getParameter("email");
        String endereco = request.getParameter("endereco");
        String dataNascimento = request.getParameter("dataNascimento");
    }
}

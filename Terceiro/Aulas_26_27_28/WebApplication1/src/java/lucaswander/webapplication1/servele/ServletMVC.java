/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.webapplication1.servele;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.webapplication.logic.Logica;

/**
 *
 * @author strudel
 */

@WebServlet("/mvc")
public class ServletMVC extends HttpServlet{
    
    protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException{
        String parametro = request.getParameter("logica");
        
        String nomeDaClasse = "lucaswander.webapplication.logic." + parametro;
        
        try{
            Class classe = Class.forName(nomeDaClasse);
            Logica logica = (Logica)classe.newInstance();
            String pagina = logica.executa(request, response);
            
            request.getRequestDispatcher(pagina).forward(request, response);
        }
        catch(Exception e){
            throw new ServletException("A logica de negocios causou uma excecao");
            
        }
    }
}

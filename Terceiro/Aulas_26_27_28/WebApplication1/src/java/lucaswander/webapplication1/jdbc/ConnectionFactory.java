package lucaswander.webapplication1.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    
    public Connection getConnection() throws ClassNotFoundException{
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            return  DriverManager . getConnection ("jdbc:mysql://150.164.102.160/daw-aluno1","daw-aluno1", "lucas");
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
}

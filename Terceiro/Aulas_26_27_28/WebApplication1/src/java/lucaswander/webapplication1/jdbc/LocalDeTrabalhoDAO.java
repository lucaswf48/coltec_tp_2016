package lucaswander.webapplication1.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LocalDeTrabalhoDAO implements Formulario{
    
    private Connection connection;
    
    public LocalDeTrabalhoDAO() throws ClassNotFoundException{
        this.connection = new ConnectionFactory().getConnection();
    }
    
    
    

    @Override
    public void insere(Object ob) {
                
        LocalDeTrabalho local = (LocalDeTrabalho) ob;
        String insere = "INSERT INTO LocalDeTrabalho (nome,endereco) values (?,?)";

        try{
            PreparedStatement stmt = this.connection.prepareStatement(insere);
            stmt.setString(1, local.getNome());
            stmt.setString(2, local.getEndereco());
            stmt.execute();
            stmt.close();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void altera(Object ob) throws SQLException{
        
        LocalDeTrabalho local = (LocalDeTrabalho) ob;
        
        PreparedStatement stmt = connection.prepareStatement("update contatos set nome = ? , endereco = ? where id = ?");
        
        stmt.setString(1,local.getNome());
        stmt.setString(2,local.getEndereco());
        stmt.setInt(3, local.getId());
        
        stmt.execute();
        stmt.close();
    }

    @Override
    public void remove(Object ob) {
        
        LocalDeTrabalho local = (LocalDeTrabalho) ob;
        
        try {
            PreparedStatement stmt = connection.prepareStatement("delete from LocalDeTrabalho where id = ?");
            
            stmt.setInt(1,local.getId());
            stmt.execute();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(LocalDeTrabalhoDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public List localiza(String nome) throws SQLException{
        
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from LocalDeTrabalho where nome like ?");

        stmt.setString(1, nome);

        ResultSet rs = stmt.executeQuery();

        List<LocalDeTrabalho> local = new ArrayList<LocalDeTrabalho>();

        while(rs.next()){
            LocalDeTrabalho localDeTrabalho = new LocalDeTrabalho();
            localDeTrabalho.setEndereco(rs.getString("endereco"));
            localDeTrabalho.setNome(rs.getString("nome"));
            localDeTrabalho.setId(rs.getInt("id"));

            local.add(localDeTrabalho);
        }
      
        rs.close();
        stmt.close();
            
        return local;
    }

    @Override
    public List Listar() throws SQLException {
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from LocalDeTrabalho;");
        ResultSet rs = stmt.executeQuery();
        
        List<LocalDeTrabalho> local = new ArrayList<LocalDeTrabalho>();
        
        
        while(rs.next()){
            
            LocalDeTrabalho localDeTrabalho = new LocalDeTrabalho();
            localDeTrabalho.setEndereco(rs.getString("endereco"));
            localDeTrabalho.setNome(rs.getString("nome"));
            localDeTrabalho.setId(rs.getInt("id"));

            local.add(localDeTrabalho);
        }
        
        rs.close();
        stmt.close();
        return local;
    }
    
       
}

package lucaswander.webapplication1.jdbc;

import java.sql.SQLException;
import java.util.List;

public interface Formulario {
    
    public void insere(Object ob) throws SQLException;

    public void altera(Object ob) throws SQLException;

    public void remove(Object ob) throws SQLException;
    
    public List localiza(String nome) throws SQLException;
    
    public List Listar() throws SQLException;   
}

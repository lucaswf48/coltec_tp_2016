package lucaswander.webapplication1.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;



public class ContatoDAO {
    
    private Connection connection;
    
    public ContatoDAO() throws ClassNotFoundException{
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void adiciona(Contato contato)throws SQLException{
        
        String insert = "insert into contatos " + "(nome,email,endereco,dataNascimento,last_name,id_localdetrabalho)" + " values (?,?,?,?,?,?)";
        
        
            PreparedStatement stmt = this.connection.prepareStatement(insert);
            stmt.setString(1,contato.getNome());
            stmt.setString(2,contato.getEmail());
            stmt.setString(3,contato.getEndereco());
            stmt.setString(4,contato.getDataNascimento());
            stmt.setString(5,contato.getLastName());
            stmt.setInt(6,contato.getLocalDeTrabalhoId());
            stmt.execute();
            stmt.close();
        
            
    }

    public List<Contato> localizaLocalDeTrabalho(int i) throws SQLException{
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        PreparedStatement stmt = this.connection.prepareStatement("SELECT * FROM contatos JOIN LocalDeTrabalho ON contatos.id_localdetrabalho = LocalDeTrabalho.id");
        
        ResultSet rs = stmt.executeQuery();
        
        while(rs.next()){
            
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
        
    
    public List<Contato> getSelec() throws SQLException{
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos");
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    public List<Contato> localizaContatoPrimeiroNome(String nome) throws SQLException{
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos where nome like ?");
        
        
        stmt.setString(1, nome);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    public List<Contato> localizaContatoLastName(String nome) throws SQLException{
        PreparedStatement stmt = this.connection.prepareStatement("select * from contatos where last_name like ?");
        
        
        stmt.setString(1, nome);
        
        ResultSet rs = stmt.executeQuery();
        
        List<Contato> contatos = new ArrayList<Contato>();
        
        while(rs.next()){
            Contato contato = new Contato();
            contato.setDataNascimento(rs.getString("dataNascimento"));
            contato.setEmail(rs.getString("email"));
            contato.setEndereco(rs.getString("endereco"));
            contato.setNome(rs.getString("nome"));
            contato.setLastName(rs.getString("last_name"));
            contato.setId(rs.getInt("id"));
            contatos.add(contato);
        }
        
        rs.close();
        stmt.close();
        return contatos;
    }
    
    
    public void alteraContato(Contato contato)throws SQLException{
        PreparedStatement stmt = connection.prepareStatement("update contatos set nome = ? , email = ? , endereco = ?,dataNascimento = ? ,last_name = ? where id = ?");
        
        stmt.setString(1,contato.getNome());
        stmt.setString(2,contato.getEmail());
        stmt.setString(3,contato.getEndereco());
        stmt.setString(4,contato.getDataNascimento());
        stmt.setString(5,contato.getLastName());
        stmt.setInt(6, contato.getId());
        stmt.execute();
        stmt.close();
    }
    
    public void removeContato(Contato contato) throws SQLException{
        
        PreparedStatement stmt = connection.prepareStatement("delete from contatos where id = ?");
        
        stmt.setInt(1,contato.getId());
        stmt.execute();
        stmt.close();
    }
    
    
   
}

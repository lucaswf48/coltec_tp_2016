/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.webapplication.logic;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.webapplication1.jdbc.Contato;
import lucaswander.webapplication1.jdbc.ContatoDAO;

/**
 *
 * @author strudel
 */
public class ListaContatosLogic implements Logica{

    @Override
    public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
        
        List<Contato> contatos = new ContatoDAO().getSelec();
        
        req.setAttribute("contatos", contatos);
        
        return "WEB-INF/jsp/lista-contatos-scriptlet.jsp";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.webapplication.logic;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.webapplication1.jdbc.Contato;
import lucaswander.webapplication1.jdbc.ContatoDAO;

/**
 *
 * @author strudel
 */
public class RemoveContatoLogic implements Logica{

    @Override
    public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
        
        long id = Long.parseLong(req.getParameter("id"));
        
        Contato contato = new Contato();
        contato.setId((int) id);
        ContatoDAO dao = new ContatoDAO();
        dao.removeContato(contato);
        
        System.out.println("Excluindo contato....");
        
        return "mvc?logica=ListaContatosLogic";
    }
}

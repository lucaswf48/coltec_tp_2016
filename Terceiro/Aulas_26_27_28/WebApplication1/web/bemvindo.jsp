<%-- 
    Document   : bemvindo
    Created on : 20/09/2016, 08:07:25
    Author     : strudel
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%-- comentário em JSP--%>
        <% 
            String mensagem = "Bem vindo!";
        %>
        <% 
            out.println(mensagem);
        %>
        <br/>
        <%
            String desenvolvido = "Desenvolvido por Lucas Wander";
        %>
        
        <%= desenvolvido %>
        
        <br/>
        
        <% 
            System.out.println("Tudo foi executado!");
        %>
        
        <h1>Hello World!</h1>
    </body>
</html>

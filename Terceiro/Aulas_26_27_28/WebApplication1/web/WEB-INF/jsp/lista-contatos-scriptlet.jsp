<%-- 
    Document   : lista-contatos-scriptlet
    Created on : 20/09/2016, 08:15:05
    Author     : strudel
--%>

<%@page import="java.util.List"%>
<%@page import="lucaswander.webapplication1.jdbc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table,th,td{
                border: 1px black solid;
            }
        </style>
    </head>
    <body>
        <table>
        <c:import url="cabecalho.jsp"/>
            <c:forEach var="contato" items="${contatos}">
                <tr>
                <td>${contato.nome}</td>
            
             <td>
                    <c:if test="${not empty contato.email}">
                        <a href="mailto:${contato.email}">${contato.email}</a>
                    </c:if>
                    <c:if test="${empty contato.email}">
                        E-mail não informado.
                    </c:if>
                </td>
                
                
                <td>${contato.endereco}</td>
                <td>${contato.dataNascimento}</td>
               <td><a href="mvc?logica=RemoveContatoLogic&id=${contato.id}">Remover</a></td>           
                </tr>
            </c:forEach>
           
                
            
        </table>
            <c:import url="rodape.jsp"/>
    </body>
</html>

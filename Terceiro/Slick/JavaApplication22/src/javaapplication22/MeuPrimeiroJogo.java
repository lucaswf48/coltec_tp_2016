package javaapplication22;

import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

public class MeuPrimeiroJogo extends BasicGame{

    private TiledMap map;
    
    public MeuPrimeiroJogo(String title) {
        super(title);
    }

    @Override
    public void init(GameContainer gc) throws SlickException {
        this.map = new TiledMap("examples/kek.tmx");
    }

    @Override
    public void update(GameContainer gc, int i) throws SlickException {
       // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void render(GameContainer gc, Graphics grphcs) throws SlickException {
        this.map.render(0, 0);
    }

   
    
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  lucas
 * Created: Oct 11, 2016
 */

CREATE DATABASE IF NOT EXISTS ficha_policial
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_general_ci;


USE ficha_policial;



CREATE TABLE IF NOT EXISTS estado(

    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    
    CONSTRAINT pk_localizacao PRIMARY KEY (id)

)DEFAULT charset = utf8;


CREATE TABLE IF NOT EXISTS fichapolicial(

    id INT NOT NULL AUTO_INCREMENT,
    nome VARCHAR(45) NOT NULL,
    cpf VARCHAR(11) NOT NULL,
    residencia VARCHAR(45) NOT NULL,
    tipo_delito VARCHAR(45) NOT NULL,
    data_crime VARCHAR(45)NOT NULL,
    hora_crime VARCHAR(45)NOT NULL,
    local_crime VARCHAR(45)NOT NULL,
    id_estado INT NOT NULL,

    CONSTRAINT pk_fichapolicial PRIMARY KEY (id),
   
     CONSTRAINT fk_estado_fichapolicial FOREIGN KEY (id_estado)
    REFERENCES estado(id)
    ON DELETE CASCADE

)DEFAULT charset = utf8;

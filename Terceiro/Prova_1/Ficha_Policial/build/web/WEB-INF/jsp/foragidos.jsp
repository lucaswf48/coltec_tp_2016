<%-- 
    Document   : foragidos
    Created on : Oct 11, 2016, 10:12:00 AM
    Author     : lucas
--%>
<%@page import="java.util.List"%>
<%@page import="lucaswander.fichapolicial.jdbc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>
            table,th,td{
                border: 1px black solid;
            }
        </style>
    </head>
    <body>
        
    <table>
        <tr>
            <th>Nome</th>
            <th>cpf</th>
            <th>residencia</th>
            <th>tipoDelito</th>
            <th>dataCrime</th>
            <th>horaCrime</th>
            <th>localCrime</th>
            <th>id_estado</th>
            <th>id</th>
        </tr>
        <c:forEach var="ficha" items="${fichas}">
                <tr>
                <td>
                    ${ficha.nome}
                </td>
            
                <td>
                    ${ficha.cpf}
                </td>
                <td>
                    ${ficha.residencia}
                </td>
                <td>
                    ${ficha.tipoDelito}
                </td>
                <td>
                    ${ficha.dataCrime}
                </td>
                <td>
                    ${ficha.horaCrime}
                </td>
                <td>
                    ${ficha.localCrime}
                </td>
                <td>
                    ${ficha.id_estado}
                </td>
                
                <td>
                    ${ficha.id}
                </td>
                
                

                </tr>
            </c:forEach>                       
        </table>
    </body>
</html>

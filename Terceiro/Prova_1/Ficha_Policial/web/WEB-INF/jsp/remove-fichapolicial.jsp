<%-- 
    Document   : remove-fichapolicial
    Created on : Oct 11, 2016, 10:08:02 AM
    Author     : lucas
--%>
<%@page import="java.util.List"%>
<%@page import="lucaswander.fichapolicial.jdbc.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <style>
            table,th,td{
                border: 1px black solid;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
                 <tr>
            <th>Nome</th>            
            <th>id</th>
        </tr>
         <c:forEach var="ficha" items="${fichas}">
                <tr>
                <td>
                    ${ficha.nome}
                </td>
                <td><a href="mvc?logica=DeletaLogica&id=${ficha.id}">Remover</a></td>           
                </tr>
            </c:forEach>                       
        </table>
    </body>
</html>

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.jdbc;

/**
 *
 * @author lucas
 */
public class FichaPolicial {
    
    private String nome;
    private String cpf;
    private String residencia;
    private String tipoDelito;
    private String dataCrime;
    private String horaCrime;
    private String localCrime;
    private int id_estado;
    private int id;

    public FichaPolicial() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getResidencia() {
        return residencia;
    }

    public void setResidencia(String residencia) {
        this.residencia = residencia;
    }

    public String getTipoDelito() {
        return tipoDelito;
    }

    public void setTipoDelito(String tipoDelito) {
        this.tipoDelito = tipoDelito;
    }

    public String getDataCrime() {
        return dataCrime;
    }

    public void setDataCrime(String dataCrime) {
        this.dataCrime = dataCrime;
    }

    public String getHoraCrime() {
        return horaCrime;
    }

    public void setHoraCrime(String horaCrime) {
        this.horaCrime = horaCrime;
    }

    public String getLocalCrime() {
        return localCrime;
    }

    public void setLocalCrime(String localCrime) {
        this.localCrime = localCrime;
    }

    public int getId_estado() {
        return id_estado;
    }

    public void setId_estado(int id_estado) {
        this.id_estado = id_estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    
}

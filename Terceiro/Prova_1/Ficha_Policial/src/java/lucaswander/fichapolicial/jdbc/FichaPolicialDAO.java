/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.jdbc;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author lucas
 */
public class FichaPolicialDAO extends DefaultDAO{
    
    
    public FichaPolicialDAO() throws ClassNotFoundException{
        super();
    }
    
    @Override
    public void insere(Object ob) throws SQLException {
        //TODO:insere
        
        FichaPolicial ficha = (FichaPolicial)ob;
        
        String insert = "INSERT INTO fichapolicial (nome,cpf,residencia,tipo_delito,data_crime,hora_crime,local_crime,id_estado) VALUES (?,?,?,?,?,?,?,?)";
        //System.out.println();
        PreparedStatement stmt = this.connection.prepareStatement(insert);
        stmt.setString(1, ficha.getNome());
        stmt.setString(2, ficha.getCpf());
        stmt.setString(3, ficha.getResidencia());
        stmt.setString(4, ficha.getTipoDelito());
        stmt.setString(5, ficha.getDataCrime());
        stmt.setString(6, ficha.getHoraCrime());
        stmt.setString(7, ficha.getLocalCrime());
        stmt.setInt(8,ficha.getId_estado());
        System.out.println(ficha.getId_estado());
        stmt.execute();
        
        stmt.close();
    }

    @Override
    public void altera(Object ob) throws SQLException {
        //TODO:altera
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void remove(Object ob) throws SQLException {
        //TODO:remove
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List localiza(String nome) throws SQLException {
        //TODO:localiza
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public List Listar() throws SQLException {
        //TODO:Listar
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    public List getListar() throws SQLException {
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from fichapolicial ORDER BY nome");
        
        ResultSet rs = stmt.executeQuery();
        
        List<FichaPolicial> fichas = new ArrayList<FichaPolicial>();
        
        while(rs.next()){
            FichaPolicial ficha = new FichaPolicial();
       
            ficha.setId(rs.getInt("id"));
            ficha.setNome(rs.getString("nome"));
            ficha.setCpf(rs.getString("cpf"));
            ficha.setResidencia(rs.getString("residencia"));
            ficha.setTipoDelito(rs.getString("tipo_delito"));
            ficha.setDataCrime(rs.getString("data_crime"));
            ficha.setHoraCrime(rs.getString("hora_crime"));
            ficha.setLocalCrime(rs.getString("local_crime"));
            ficha.setId_estado(rs.getInt("id_estado"));

            fichas.add(ficha);
        }
        
        rs.close();
        stmt.close();
        return fichas;
    }
    
    public List getListarForagidos() throws SQLException {
        
        PreparedStatement stmt = this.connection.prepareStatement("select * from fichapolicial WHERE id_estado = ?");
        stmt.setInt(1, 3);
        ResultSet rs = stmt.executeQuery();
        
        List<FichaPolicial> fichas = new ArrayList<FichaPolicial>();
        
        while(rs.next()){
            FichaPolicial ficha = new FichaPolicial();
       
            ficha.setId(rs.getInt("id"));
            ficha.setNome(rs.getString("nome"));
            ficha.setCpf(rs.getString("cpf"));
            ficha.setResidencia(rs.getString("residencia"));
            ficha.setTipoDelito(rs.getString("tipo_delito"));
            ficha.setDataCrime(rs.getString("data_crime"));
            ficha.setHoraCrime(rs.getString("hora_crime"));
            ficha.setLocalCrime(rs.getString("local_crime"));
            ficha.setId_estado(rs.getInt("id_estado"));

            fichas.add(ficha);
        }
        
        rs.close();
        stmt.close();
        return fichas;
    }


    public void removeContato(FichaPolicial ficha) throws SQLException {
            
        PreparedStatement stmt = connection.prepareStatement("delete from fichapolicial where id = ?");
        
        stmt.setInt(1,ficha.getId());
        stmt.execute();
        stmt.close();
        
    }
    
}

package lucaswander.fichapolicial.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionFactory {
    
    public Connection getConnection() throws ClassNotFoundException{
        
        try{
            Class.forName("com.mysql.jdbc.Driver");
            return  DriverManager . getConnection ("jdbc:mysql://alunos.coltec.ufmg.br:3306/daw-aluno1","daw-aluno1", "lucas");
        }
        catch(SQLException e){
            throw new RuntimeException(e);
        }
    }
}

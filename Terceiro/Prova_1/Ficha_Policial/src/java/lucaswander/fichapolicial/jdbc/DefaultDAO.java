/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.jdbc;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author lucas
 */
public abstract class DefaultDAO {
    
    protected Connection connection;
    
    public DefaultDAO() throws ClassNotFoundException{
        this.connection = new ConnectionFactory().getConnection();
    }
    
    public void fechaConnection() throws SQLException{
        this.connection.close();
    }
    
    public abstract void insere(Object ob) throws SQLException;

    public abstract void altera(Object ob) throws SQLException;

    public abstract void remove(Object ob) throws SQLException;
    
    public abstract List localiza(String nome) throws SQLException;
    
    public abstract List Listar() throws SQLException; 
}

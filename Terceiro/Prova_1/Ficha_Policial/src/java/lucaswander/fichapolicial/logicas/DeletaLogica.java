/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.logicas;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.fichapolicial.jdbc.FichaPolicial;
import lucaswander.fichapolicial.jdbc.FichaPolicialDAO;

/**
 *
 * @author lucas
 */
public class DeletaLogica implements Logica{

    @Override
    public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
        
        long id = Long.parseLong(req.getParameter("id"));        
        FichaPolicial contato = new FichaPolicial();
        contato.setId((int) id);
        FichaPolicialDAO dao = new FichaPolicialDAO();
       
        dao.removeContato(contato);
        
        System.out.println("Excluindo ficha....");
        
        return "mvc?logica=ListaLogica";
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.logicas;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.fichapolicial.jdbc.FichaPolicial;
import lucaswander.fichapolicial.jdbc.FichaPolicialDAO;

/**
 *
 * @author lucas
 */
public class ListaLogicaForagidos implements Logica{

    @Override
    public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
        
        List<FichaPolicial> fichas = new FichaPolicialDAO().getListarForagidos();
       
        req.setAttribute("fichas", fichas);
        
        return "WEB-INF/jsp/foragidos.jsp";
    }
    
}

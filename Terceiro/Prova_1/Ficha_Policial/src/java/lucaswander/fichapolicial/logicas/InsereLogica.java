/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.fichapolicial.logicas;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lucaswander.fichapolicial.jdbc.FichaPolicial;
import lucaswander.fichapolicial.jdbc.FichaPolicialDAO;

/**
 *
 * @author lucas
 */
public class InsereLogica implements Logica{

    @Override
    public String executa(HttpServletRequest req, HttpServletResponse res) throws Exception {
  
        FichaPolicial ficha = new FichaPolicial();
        
        ficha.setNome(req.getParameter("nome"));
        ficha.setCpf(req.getParameter("cpf"));
        ficha.setResidencia(req.getParameter("residencia"));
        ficha.setTipoDelito(req.getParameter("tipo_delito"));
        ficha.setDataCrime(req.getParameter("data_crime"));
        ficha.setHoraCrime(req.getParameter("hora_crime"));
        ficha.setLocalCrime(req.getParameter("local_crime"));
        ficha.setId_estado(Integer.parseInt(req.getParameter("id_estado")));
        
        FichaPolicialDAO fichaDAO = new FichaPolicialDAO();
        
        fichaDAO.insere(ficha);
        System.out.println("dasdasdasdas");
        
       return "mvc?logica=ListaLogica";
    }
    
    
}

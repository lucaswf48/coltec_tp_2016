/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisitemafabrica;

/**
 *
 * @author Lucas
 */
public class FrabricaDeBombons {
    
    private Bombom[] bombons = new Bombom[0];
    
    public void adicionaBombom(Bombom b){
        
        Bombom[] novo = new Bombom[this.bombons.length+1];
        
        System.arraycopy(this.bombons, 0, novo, 0, this.bombons.length);
        
        novo[this.bombons.length] = b;
        
        this.bombons = novo;
    }

    public Bombom[] getBombons() {
        return bombons;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sisitemafabrica;

/**
 *
 * @author Lucas
 */
public class Bombom {

    private static int numeroDeBombons = 0;

    
    private int numeroDeSerie;
    private String[] ingredientes = new String[6];
    
    Bombom(){
        this.numeroDeSerie = numeroDeBombons;
        numeroDeBombons++;
    }

    public static int getNumeroDeBombons() {
        return numeroDeBombons;
    }

    
    public int getNumeroDeSerie() {
        return numeroDeSerie;
    }
    
    public void adicionaIngrediente(String s){
        
        for(String ss : ingredientes){
            if(ss.equals(s)){
                System.out.println("Ingrediente repetido.");
                return;
            }
        }
        
        for(int i = 0; i < this.ingredientes.length; i++){
            if(this.ingredientes[i] == null){
                this.ingredientes[i] = s;
                return;
            }
        }
        
        System.out.println("Maximo de ingredientes.");
    }

    public String[] getIngredientes() {
        return ingredientes;
    }
        
}

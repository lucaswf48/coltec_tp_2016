/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaacademico;

/**
 *
 * @author Lucas
 */
public class Curso {
    
    private int codigo;
    private String descricao;
    private Professor coordenador;
    private Disciplina[] disciplinas = new Disciplina[0];
    private Professor[] professores = new Professor[0];
    private Turma[] turmas = new Turma[0];
    
    public void adicionaDisciplina(Disciplina d){
        
        Disciplina[] aux = new Disciplina[this.disciplinas.length + 1];
        
        System.arraycopy(this.disciplinas, 0, aux, 0, this.disciplinas.length);
        
        aux[this.disciplinas.length] = d;
        
        this.disciplinas = aux;
    }
    
    public void adicionaProfessores(Professor p){
        
        Professor[] aux = new Professor[this.professores.length + 1];
        
        System.arraycopy(this.professores, 0, aux, 0, this.professores.length);
        
        aux[this.professores.length] = p;
        
        this.professores = aux;
    }
        
    public void adicionaTurma(Turma t){
        
        Turma[] aux = new Turma[this.turmas.length + 1];
        
        System.arraycopy(this.turmas, 0, aux, 0, this.turmas.length);
        
        aux[this.turmas.length] = t;
        
        this.turmas = aux;
    }
    
    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Professor getCoordenador() {
        return coordenador;
    }

    public void setCoordenador(Professor coordenador) {
        this.coordenador = coordenador;
    }

    public Disciplina[] getDisciplinas() {
        return disciplinas;
    }

    public Professor[] getProfessores() {
        return professores;
    }

    public Turma[] getTurmas() {
        return turmas;
    }
    
}

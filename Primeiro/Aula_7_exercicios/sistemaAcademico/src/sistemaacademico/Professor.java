/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaacademico;

/**
 *
 * @author Lucas
 */
public class Professor {
    
    private String titulacaoMaxima;
    private Curso[] curso = new Curso[0];
    private Turma[] turmas = new Turma[0];

    public String getTitulacaoMaxima() {
        return titulacaoMaxima;
    }

    public void setTitulacaoMaxima(String titulacaoMaxima) {
        this.titulacaoMaxima = titulacaoMaxima;
    }
    
    public void adicionaCurso(Curso t){
        
        Curso[] aux = new Curso[this.curso.length + 1];
        
        System.arraycopy(this.curso, 0, aux, 0, this.curso.length);
        
        aux[this.curso.length] = t;
        
        this.curso = aux;
    }

    public Curso[] getCurso() {
        return curso;
    }
    
    public void adicionaTurma(Turma t){
        
        Turma[] aux = new Turma[this.turmas.length + 1];
        
        System.arraycopy(this.turmas, 0, aux, 0, this.turmas.length);
        
        aux[this.turmas.length] = t;
        
        this.turmas = aux;
    }

    public Turma[] getTurma() {
        return turmas;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaacademico;

/**
 *
 * @author Lucas
 */
public class Disciplina {
    
    private Curso curso;
    private int codigo;
    private int cargaHoraria;
    private String descricao;
    private String ementa;
    private String bibliografia;
    private Professor[] professores = new Professor[0];
    private Disciplina[] preRequisitos = new Disciplina[0];
    
    

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public int getCargaHoraria() {
        return cargaHoraria;
    }

    public void setCargaHoraria(int cargaHoraria) {
        this.cargaHoraria = cargaHoraria;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getEmenta() {
        return ementa;
    }

    public void setEmenta(String ementa) {
        this.ementa = ementa;
    }

    public String getBibliografia() {
        return bibliografia;
    }

    public void setBibliografia(String bibliografia) {
        this.bibliografia = bibliografia;
    }
    
    public void adicionaPreRequisito(Disciplina d){
        
        Disciplina[] aux = new Disciplina[this.preRequisitos.length + 1];
        
        System.arraycopy(this.preRequisitos, 0, aux, 0, this.preRequisitos.length);
        
        aux[this.preRequisitos.length] = d;
        
        this.preRequisitos = aux;
    }
    
    
    public Disciplina[] getPreRequisitos() {
        return preRequisitos;
    }
    
    
    public void adicionaProfessores(Professor p){
        
        Professor[] aux = new Professor[this.professores.length + 1];
        
        System.arraycopy(this.professores, 0, aux, 0, this.professores.length);
        
        aux[this.professores.length] = p;
        
        this.professores = aux;
    }

    public Professor[] getProfessores() {
        return professores;
    }
    
}

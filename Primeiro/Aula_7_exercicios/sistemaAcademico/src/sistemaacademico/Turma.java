/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaacademico;

/**
 *
 * @author Lucas
 */
public class Turma {

    private int ano;
    private int semestre;
    private int diasSemana;
    private String horarios;
    private Disciplina disciplina;
    private Aluno[] alunos = new Aluno[0];
    private Avaliacao[] avalicoes = new Avaliacao[0];

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public int getSemestre() {
        return semestre;
    }

    public void setSemestre(int semestre) {
        this.semestre = semestre;
    }

    public int getDiasSemana() {
        return diasSemana;
    }

    public void setDiasSemana(int diasSemana) {
        this.diasSemana = diasSemana;
    }

    public String getHorarios() {
        return horarios;
    }

    public void setHorarios(String horarios) {
        this.horarios = horarios;
    }

    public Disciplina getDisciplina() {
        return disciplina;
    }

    public void setDisciplina(Disciplina disciplina) {
        this.disciplina = disciplina;
    }
    
    public void adicionaAluno(Aluno a){
        
        Aluno[] aux = new Aluno[this.alunos.length + 1];
        
        System.arraycopy(this.alunos, 0, aux, 0, this.alunos.length);
        
        aux[this.alunos.length] = a;
        
        this.alunos = aux;
    }
    
    public Aluno[] getAlunos() {
        return alunos;
    }

    public void adicionaAluno(Avaliacao a){
        
        Avaliacao[] aux = new Avaliacao[this.avalicoes.length + 1];
        
        System.arraycopy(this.avalicoes, 0, aux, 0, this.avalicoes.length);
        
        aux[this.avalicoes.length] = a;
        
        this.avalicoes = aux;
    }
    
    public Avaliacao[] getAvalicoes() {
        return avalicoes;
    }
    
}

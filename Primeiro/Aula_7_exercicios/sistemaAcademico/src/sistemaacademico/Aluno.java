/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemaacademico;

/**
 *
 * @author Lucas
 */
public class Aluno {
    
    private String matricula;
    private Curso curso;
    private String situacao;
    private Turma[] turmas = new Turma[0];
    
    public void adicionaTurma(Turma t){
        
        Turma[] aux = new Turma[this.turmas.length + 1];
        
        System.arraycopy(this.turmas, 0, aux, 0, this.turmas.length);
        
        aux[this.turmas.length] = t;
        
        this.turmas = aux;
    }
    
    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public Curso getCurso() {
        return curso;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }

    public Turma[] getTurmas() {
        return turmas;
    }
    
    
}

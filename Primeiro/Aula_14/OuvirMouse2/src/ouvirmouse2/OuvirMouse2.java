/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ouvirmouse2;

import javax.swing.JFrame;

/**
 *
 * @author Lucas
 */
public class OuvirMouse2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        JFrame frame = new JFrame ("Testando movimentos do Mouse com Adapter para MousePressed");
        MouseSpy ouvinte = new MouseSpy ();
        frame. setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.addMouseListener(ouvinte);
        frame.setBounds (399 ,0 ,400 ,300);
        frame.setVisible(true);
    }
    
}

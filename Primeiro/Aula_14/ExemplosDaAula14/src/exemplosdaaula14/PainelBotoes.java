/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplosdaaula14;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 *
 * @author Lucas
 */
public class PainelBotoes extends JPanel implements ActionListener{
    
      class OuvinteAmarelo implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent evt){
            Color color = Color.yellow;
            setBackground(color);
            repaint();
        }
    }
    
    
    class OuvinteAzul implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent evt){
            Color color = Color.yellow;
            setBackground(color);
            repaint();
        }
    }
    
  
    
    class OuvinteVermelho implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent evt){
            Color color = Color.yellow;
            setBackground(color);
            repaint();
        }
    }
    
    
    
    private static final long serialVersionUID = 1L;
    
    private JButton botaoAmarelo;
    private JButton botaoAzul;
    private JButton botaoVermelho;
    
    public PainelBotoes(){
        
        botaoAmarelo = new JButton("Amerelo");
        this.add(botaoAmarelo);
        botaoAmarelo.addActionListener(new OuvinteAmarelo());
        
        botaoAzul = new JButton("Azul");
        add(botaoAzul);
        botaoAzul.addActionListener(new OuvinteAzul());
        //botaoAzul.addActionListener(new ImprimeConsole());
        
        botaoVermelho  = new JButton("Vermelho");
        add(botaoVermelho);
        botaoVermelho.addActionListener(new OuvinteVermelho());
        
    }
    
    public void actionPerformed(ActionEvent evt){
        
        Object source = evt.getSource();
        
        Color color = getBackground();
        if(source == botaoAmarelo)
            color = Color.yellow;
        else if(source == botaoAzul)
            color = Color.blue;
        else if(source == botaoVermelho)
            color = Color.red;
        
        setBackground(color);
        repaint();
    }
    
    
}

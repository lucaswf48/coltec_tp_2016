/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplosdaaula14;

import javax.swing.JFrame;

/**
 *
 * @author Lucas
 */
public class ExemplosDaAula14 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JanelaBotoes frame = new JanelaBotoes();
        ExemplosDaAula14 gui = new ExemplosDaAula14();
        
        gui.go();
    }
    
    public void go(){
        JFrame frame = new JFrame();
        
        frame.setTitle("Teste de Botoes");
        frame.setSize(450, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new PainelBotoes());
        frame.setVisible(true);
        
        
    }
    
}

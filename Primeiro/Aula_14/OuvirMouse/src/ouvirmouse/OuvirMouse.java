/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ouvirmouse;

import javax.swing.JFrame;

/**
 *
 * @author Lucas
 */
public class OuvirMouse {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        JFrame frame = new JFrame("Testando movimentos do Mouse");
        frame. setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        MouseSpy ouvinte = new MouseSpy ();
        frame.addMouseListener(ouvinte);
        frame. addMouseMotionListener (ouvinte);
        frame.setBounds (399 ,0 ,400 ,300);
        frame.setVisible(true);
    }
    
}

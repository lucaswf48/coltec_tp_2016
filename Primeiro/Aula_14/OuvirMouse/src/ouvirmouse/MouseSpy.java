/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ouvirmouse;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
/**
 *
 * @author Lucas
 */
class MouseSpy implements MouseListener , MouseMotionListener{
    
    @Override
    public void mousePressed(MouseEvent event){
        System.out.println(" Mouse : Foi pressionado o botao no. " + event.getButton()+ " em x = "+ event.getX() + " y = " + event.getY());
    }
    
    @Override
    public void mouseReleased(MouseEvent event){
        System.out.println("Mouse: Foi solto o botao no. " + event.getButton() + " em x = " + event.getX() + " y = " + event.getY());
    }
    
    @Override
    public void mouseClicked(MouseEvent event){
        System.out.println("Mouse: foi clicado o botao no. " + event.getButton() + "em x = " + event.getX() + " y = " + event.getY());
    }
    
    @Override
    public void mouseEntered(MouseEvent event){
        System.out.println("Mouse: cursor entrou na janela em = " + event.getX() + " y = " + event.getY());
 }

    @Override
    public void mouseExited(MouseEvent event){
        System.out.println("Mouse: cursor saiu da janela em x = " + event.getX() + " y = " + event.getY());
 }

    // metodos da interface MouseMotionListener
    @Override
    public void mouseDragged(MouseEvent event){
        System.out.println("Mouse no. " + event.getButton() + " arrastado em " + event.getX() + " y = " + event.getY());
 }

    @Override
    public void mouseMoved(MouseEvent event){
        System.out.println("Mouse moveu em " + event.getX() + " y = " + event.getY());
    }
 
}

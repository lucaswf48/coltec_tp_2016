/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinotas;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import javax.swing.JFileChooser;

/**
 *
 * @author Lucas
 */
public class Relatorio {

    
    
    static public void escreveNoArquivo(File f,double media, Aluno[] a) throws FileNotFoundException{
        
        Scanner s = new Scanner(System.in);
        PrintStream ps = new PrintStream(f);
        
        JFileChooser file = new JFileChooser () ;
        file.showOpenDialog(null);
        
        File fi = file.getSelectedFile();
        
        PrintStream ps1 = new PrintStream(fi);
        
        String aux = "";
        
        for(int i = 0; i < a.length; ++i){
            
            ps.println(a[i].getNome() + " " + a[i].getMedia() + " "  + (a[i].getMedia() - media));
            if(a[i].getMedia() >= 60 )
                aux = "Aprovado";
            else
                aux = "Reprovado";
            
            ps1.println(a[i].getNome() + " " + a[i].getMedia() + " "  + aux);
        }
    }
}

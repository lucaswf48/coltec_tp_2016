/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinotas;

/**
 *
 * @author lucas
 */
public class Aluno {
    
    private static int numeroDeAlunos = 0;

    
    private int idAluno;
    private String nome;
    private double[] nota = new double[3];
    private double media;
    /**
     * @return the numeroDeAlunos
     */
    public static int getNumeroDeAlunos() {
        return numeroDeAlunos;
    }
    
    
    
    public Aluno(){
        
        this.idAluno = numeroDeAlunos;
        
        numeroDeAlunos++;
    }
    
    public Aluno(String n){

        this();
        this.setNome(n); 
    }
    
    public Aluno(String n, double[] nt){
        
        
        this(n);
      
        this.setNota(nt);
        this.setMedia();
    }
    
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
       
        this.nome = nome;

    }

    /**
     * @return the nota
     */
    public double[] getNota() {
        return nota;
    }

    /**
     * @param nota the nota to set
     */
    public void setNota(double[] nota) {
        
        this.nota[0] = nota[0];
        this.nota[1] = nota[1];
        this.nota[2] = nota[2];
    }

    /**
     * @return the idAluno
     */
    public int getIdAluno() {
        return idAluno;
    }

    private void setMedia() {
       this.media = (this.nota[0] + this.nota[1] + this.nota[2])/3;
    }

    public double getMedia() {
        return media;
    }
    
    
    
}

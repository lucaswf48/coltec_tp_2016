/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinotas;

/**
 *
 * @author Lucas
 */
public class Leitura {
    
    private String nomeAux;
    private double[] notaAux = new double[3];
    //private double notaAux;
    
    public void formataLeitura(String s){
        
        int[] vet = new int[3];
        int espaco = 0;
        for(int i = 0; i < s.length(); ++i){
            if(s.charAt(i) == ' '){
                vet[espaco] = i;
                ++espaco;
            }
        }
        
        this.nomeAux = s.substring(0, vet[0]);
        
        this.notaAux[0] = Double.parseDouble(s.substring(vet[0] + 1, vet[1]));
        this.notaAux[1] = Double.parseDouble(s.substring(vet[1] + 1, vet[2]));
        this.notaAux[2] = Double.parseDouble(s.substring(vet[2] + 1, s.length()));
        
    }

    public String getNomeAux() {
        return nomeAux;
    }

    public double[] getNotaAux() {
        return notaAux;
    }
    
    
}

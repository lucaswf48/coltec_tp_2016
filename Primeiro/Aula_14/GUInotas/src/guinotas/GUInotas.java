/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinotas;

import java.io.*;
import java.util.Scanner;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;


/**
 *
 * @author lucas
 */

public class GUInotas {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here

        JFileChooser fileChooser = new JFileChooser () ;
        fileChooser.showOpenDialog(null);
        
        File file = fileChooser.getSelectedFile();
        Scanner sc = new Scanner(file);
        Leitura l = new Leitura();
        
        Estatistica est = new Estatistica();
        
        ////////////////////////////////////////////////////////////////
        
	while (sc.hasNextLine()){
            //System.out.println(sc.nextLine());
            l.formataLeitura(sc.nextLine());
            est.adicionaAluno(new Aluno(l.getNomeAux(),l.getNotaAux()));
        }
        
        est.calculaMedia();
        est.calculaVariancia();
        est.calcularDesvioPadrao();

        JOptionPane.showMessageDialog(null , "Media: " + est.getMedia() +"\n"+ "Variancia: " + est.getVariancia() + "\n"+ "Desvio Padrao: " + est.getDesvio());
        
        fileChooser.showOpenDialog(null);
        Relatorio.escreveNoArquivo(fileChooser.getSelectedFile(),est.getMedia(),est.getAlunos());
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guinotas;

import static java.lang.Math.pow;

/**
 *
 * @author lucas
 */
public class Estatistica {
    
    
    private Aluno[] alunos = new Aluno[0];
    private double mediaTurma;
    private double variancia;
    private double desvio;
    
    public Aluno[] getAlunos(){
        return this.alunos;
    }
    
    public void adicionaAluno(Aluno a){
        
        Aluno[] aux = new Aluno[this.alunos.length + 1];
        for(int i = 0; i < this.alunos.length; ++i){
            aux[i] = this.alunos[i];
        }
        
        aux[this.alunos.length] = a;
        
        this.alunos = aux;
    }
    
    
    public void imprime(){
        
        for(int i = 0; i < this.alunos.length; ++i){
            System.out.println("--------------------------------------------------");
            System.out.println("Nome: " + this.alunos[i].getNome());
            System.out.println("Nota: " + this.alunos[i].getNota());
            System.out.println("ID: " + this.alunos[i].getIdAluno());
            System.out.println("--------------------------------------------------");
        }
    }
    
    public void esta(){
        System.out.println("--------------------------------------------------");
        System.out.println("Media: " + this.getMedia());
        System.out.println("Variancia: " + this.getVariancia());
        System.out.println("Desvio: " + this.getDesvio());
        System.out.println("--------------------------------------------------");
    }
    
    public double calculaMedia(){
        
        
        for(int i = 0; i < this.alunos.length; ++i){
            
            this.mediaTurma += this.alunos[i].getMedia();
        }
        
        this.mediaTurma /= this.alunos.length;
        
        return this.getMedia();
    }
    
    public double calculaVariancia(){
        
        for(int i = 0; i < this.alunos.length; ++i){
            
            this.variancia += (pow(this.alunos[i].getMedia() - this.getMedia(),2)); 
        }
        
        this.variancia /= this.alunos.length;
       
        return this.getVariancia();
    }
    
    public double calcularDesvioPadrao(){
        
        
        this.desvio = pow(this.getVariancia(),0.5);
        
        return this.getDesvio();
    }
    
    public double getDesvio(){
        
        return this.desvio;
    }
    
    public double getVariancia(){
        
        return this.variancia;
    }
    
    public double getMedia(){
        
        return this.mediaTurma;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questao1;

import java.util.Objects;

/**
 *
 * @author strudel
 */
public class Questao1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        //É impresso fj11
        /*
        String s = "fj11";
        s.replaceAll("1", "2");
        System.out.println(s);
        */
        
        //Igualar a string horiginal ao retorno da função replaceAll()
        /*
        String s = "fj11";
        s = s.replaceAll("1", "2");
        System.out.println(s);
        */
        
        //Como fazer para saber se uma String se encontra dentro de outra
        /*
        String s = "Como fazer para saber se uma String se encontra dentro de outra";
        if(s.contains("fazer")){
            System.out.println("fa");
        }
        else{
            System.out.println("sem fazer");
        }
        */
        
        //para tirar os espaços em branco das pontas de uma String
        /*
        String s = "              para tirar os espaços em branco das pontas de uma String";
        System.out.println(s.trim());
        */
        
        //E para saber se uma String está vazia?
        /*
        String s = "";
        if(s.equals("")){
            System.out.println("daw");
        }
        */
        
        //E para saber quantos caracteres tem uma String?
        /*
        String s = "E para saber\n quantos caracteres tem uma String?";
        System.out.println(s.length());
        */
        
        // == Compara os objetos
        // equals Compara o conteudo dos objetos
        /*
        Integer x1 = new Integer(10);
        Integer x2 = new Integer(10);
        
        if(x1.equals(x2)){
            System.out.println("igual");
        }
        else{
            System.out.println("diferente");
        }
        */
        
        //Sim, a classe Integer reescreve o metodo toString(documentacao do java)
       /*
        String daw = Integer.toString(4500);
        System.out.println(daw);
       */
    }
    
}

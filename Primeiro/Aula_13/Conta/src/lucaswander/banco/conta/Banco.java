
package lucaswander.banco.conta;
/*
Crie uma classe Banco que possui um array de Conta.
Repare que num array de Conta você pode colocar tanto
ContaCorrente quanto ContaPoupanca. Crie um método
public void adiciona(Conta c), um método public Conta
pegaConta(int x) e outro public int pegaTotalDeContas(),
muito similar a relação anterior de Empresa-Funcionario.
Faça com que seu método main crie diversas contas, insira-as
no Banco e depois, com um for, percorra todas as contas do
Banco para passá-las como argumento para o
AtualizadorDeContas
*/

public class Banco {

    private Conta [] arrayContas = new Conta[1];
    
    private static int totalDeContas = 0;
    

    static public int pegaTotalDeContas(){
        
        return Banco.totalDeContas;
    }
    
    public Conta pegaConta(int x){
        
        return this.arrayContas[x];
    }
    
    
    public void adiciona(Conta c){
        
        Banco.totalDeContas++;
        
        for(int i = 0; i < this.arrayContas.length;i++){
            if(this.arrayContas[i] == null){
                this.arrayContas[i] = c;
                break;
            }
        }
        
        Conta [] aux = new Conta[this.arrayContas.length];
        
        System.arraycopy(this.arrayContas, 0, aux, 0, this.arrayContas.length);
        
        this.arrayContas = new Conta[this.arrayContas.length + 1];
        
        System.arraycopy(aux, 0, this.arrayContas, 0, aux.length);
        
        
        
    }
}


package questao1;


public class AtualizadorDeContas {
    
    private double salarioTotal = 0;
    private double selic;
    
    public AtualizadorDeContas(double selic){
        this.selic = selic;
    }
    
    public void roda(Conta c){
        
        System.out.println("Saldo anterior: " + c.getSaldo());
        c.atualiza(selic);
        System.out.println("Saldo atula: " + c.getSaldo());
        this.salarioTotal += c.getSaldo();
    }
    
    public double getSalarioTotal(){
        return this.salarioTotal;
    }
    
}


package questao1;
/*
Crie uma classe Banco que possui um array de Conta.
Repare que num array de Conta você pode colocar tanto
ContaCorrente quanto ContaPoupanca. Crie um método
public void adiciona(Conta c), um método public Conta
pegaConta(int x) e outro public int pegaTotalDeContas(),
muito similar a relação anterior de Empresa-Funcionario.
Faça com que seu método main crie diversas contas, insira-as
no Banco e depois, com um for, percorra todas as contas do
Banco para passá-las como argumento para o
AtualizadorDeContas
*/

public class Questao1 {


    public static void main(String[] args) {
        
        Banco b = new Banco();
        
        Conta c = new Conta();
        
        b.adiciona(c);
        
        c.deposita(5000);
        
        System.out.println(c.getSaldo());
        
        c.saca(500);
        
        System.out.println(c.getSaldo());
        
        try{
            c.saca(4700);
        }
        catch(ValorInvalidoException e){
            System.out.println("erro: " + e.getValorInvalido());
        }
        System.out.println(c.getSaldo());
        
        
    }
    
}

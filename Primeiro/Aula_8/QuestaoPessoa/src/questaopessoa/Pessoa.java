package questaopessoa;

abstract public class Pessoa {
    
    protected String nome;
    protected int idade;
    
    
    Pessoa(String n ,int i){
        this.nome = n;
        this.idade = i;
        
    }
    
    Pessoa(){
        this.nome = "indefinido";
        this.idade = 0;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome(String n){
        this.nome = n;
    }
    
    public int getIdade(){
        return this.idade;
    }
    
    public void setIdade(int i){
        this.idade = i;
    }

    
}

package questaopessoa;

import java.util.Scanner;


public class Agenda {
    
    Pessoa [] ag;
    
    int amigos;
    int conhecidos;
    
    Agenda(int n){
        
        this.ag = new Pessoa[n];
        
        for(int i = 0; i < this.ag.length; i++){
            int t = 1 + (int) (Math.random() * 2);
            if(t == 1){
                this.ag[i] = new Amigo();
                this.amigos++;
            }
            else if(t == 2){
                this.ag[i] = new Conhecido();
                this.conhecidos++;
            }
        }
    }
    
    public int getAmigos(){
        return this.amigos;
    }
   
    public void setAmigos(int n){
        this.amigos = n;
    }
    
    public int getConhecidos(){
        return this.conhecidos;
    }
    
    public void setConhecidos(int n){
        this.conhecidos = n;
    }
    

    public void addInformacoes(){
        
        Scanner teclado = new Scanner(System.in);
        
        for(int i = 0; i < this.ag.length; i++){
            
            System.out.println("");
            
            System.out.println("Digite o nome:");
            this.ag[i].setNome(teclado.nextLine());
            
            System.out.println("");
            
            System.out.println("Digite a idade:");
            this.ag[i].setIdade(teclado.nextInt());
            teclado.nextLine();
            
            
            
            if(this.ag[i] instanceof Amigo){ 
               
               
                Amigo daw = (Amigo)this.ag[i];
                
                System.out.println("");
                
                System.out.println("Digite a data de aniversario: ");
                daw.setAniversario(teclado.nextLine());
                this.ag[i] = daw;
                
            }
            else if(this.ag[i] instanceof Conhecido){
                
                Conhecido daw = (Conhecido)this.ag[i];
                
                System.out.println("");
                
                System.out.println("Digite o email: ");
                daw.setEmail(teclado.nextLine());
                this.ag[i] = daw;
                
            }
            
        }
    }

    
    public void imrpimeAniversario(){
        for(int i  = 0; i < this.ag.length; i++){
            
            if(this.ag[i] instanceof Amigo){
                       
                Amigo daw = (Amigo) this.ag[i];
                System.out.println("Contato: " + daw.getNome());
                System.out.println("Data de aniversario: " + daw.getAniversario());
            }
            
        }
    }

    public void imrpimeEmail(){
        for(int i  = 0; i < this.ag.length; i++){
            
            if(this.ag[i] instanceof Conhecido){
                       
                Conhecido daw = (Conhecido) this.ag[i];
                System.out.println("Contato: " + daw.getNome());
                System.out.println("Data de email: " + daw.getEmail());
            }
            
        }
    }
   
}

package questaopessoa;


public class Amigo extends Pessoa{

    protected String aniversario;
    
    Amigo(){
        this.aniversario = "indifinido";
    }
    
    public void setAniversario(String a){
        this.aniversario = a;
    }
    
    public String getAniversario(){
        return this.aniversario;
    }
}

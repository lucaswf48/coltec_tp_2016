package questaopessoa;


public class Conhecido extends Pessoa{
    
    String email;
    
    Conhecido(){
        
        this.email = "indefinido";
    }
    
    public void setEmail(String e){
        this.email = e;
    }
    
    public String getEmail(){
        return this.email;
    }
}

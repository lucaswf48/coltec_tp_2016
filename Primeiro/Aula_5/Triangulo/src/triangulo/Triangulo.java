
package triangulo;

import java.util.Scanner;

public class Triangulo {

    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Digite o numero de linha:");
        int n = teclado.nextInt();
        
        int [][] matriz = new int[n][n];
        
        for(int i = 0; i < n; i++){
            for(int j = 0; j <=i;j++){
                if(j == 0 || i == j){
                    matriz[i][j] = 1;
                }
                else{
                    matriz[i][j] = matriz[i - 1][j - 1] + matriz[i - 1][j];
                }
                System.out.print(matriz[i][j] + " ");
            }
            System.out.println("");
        }
        
    
    }
}

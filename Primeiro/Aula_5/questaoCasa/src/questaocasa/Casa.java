/*1. Vamos testar tudo o que vimos até agora com o seguinte
programa:
Classe: Casa Atributos: cor, totalDePortas, portas[]
Métodos: void pinta(String s), int
quantasPortasEstaoAbertas(), void adicionaPorta(Porta p), int
totalDePortas()
Faça o diagrama de classes desse programa.
Crie uma casa, pinte-a. Crie três portas e coloque-as na casa
através do método adicionaPorta, abra e feche-as como
desejar. Utilize o método quantasPortasEstaoAbertas para
imprimir o número de portas abertas e o método
totalDePortas para imprimir o total de portas em sua casa.
*/
package questaocasa;


public class Casa {
    
    String cor;
    int totalDePortas;
    Portas [] portas = new Portas[1];
    
    int totalDePortas(){
        
        return this.totalDePortas;
    }
    
    void adicionaPorta(Portas p){
        
        Portas [] temp = new Portas[this.portas.length];
        
        System.arraycopy(this.portas, 0, temp, 0, this.portas.length);
        
        this.portas = new Portas[this.portas.length+1];
        
        System.arraycopy(temp, 0, this.portas, 0, temp.length);
        
        for(int i = 0; i < this.portas.length;i++){
            
            if(this.portas[i] == null){
                this.portas[i] = p;
                this.totalDePortas++;
                break;
            }
        }
    }
    
    int quantasPortasAbertas(){
        
        int abertas = 0;
        
        for(int i = 0; i < this.portas.length - 1;i++){
            if(this.portas[i].situcao == true){
                abertas++;
            }
        }
        
        return abertas;
    }
    
    void pinta(String s){
        
        this.cor = s;
    }
    
    
    
}

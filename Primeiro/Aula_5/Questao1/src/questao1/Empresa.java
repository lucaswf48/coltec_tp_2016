/*1. Volte ao nosso sistema de Funcionario e crie uma classe
Empresa dentro do mesmo arquivo .java. A Empresa tem um
nome, cnpj e uma referência a um array de Funcionario, além
de outros atributos que você julgar necessário.*/
package questao1;


public class Empresa {
    
    String nome;
    String cnpj;
    Funcionario [] empregados = new Funcionario[1];
    
    
    void mostraEmpregados(){
        
        for(int i = 0; i < this.empregados.length - 1;i++){
            System.out.println("Funcionário na posição: " + (1 + i));
            System.out.println("Salario: " + this.empregados[i].salario);
        }
    }
    
    void adicionarFuncionario(Funcionario f){
        
        for(int i = 0; i < this.empregados.length; i++){
            
            if(this.empregados[i] == null){
                
                this.empregados[i] = f;
                this.arrayCheio();
                break;
            }
        }
    }
    
    void arrayCheio(){
        
        Funcionario[] tem = new Funcionario[this.empregados.length];
        
        System.arraycopy(this.empregados, 0, tem, 0, this.empregados.length);
        
        this.empregados = new Funcionario[this.empregados.length + 1];
        
        System.arraycopy(tem, 0, this.empregados, 0, tem.length);
    }
}

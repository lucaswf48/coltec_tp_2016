
package questaoestatistica;

import java.util.Arrays;


public class Estatistica {

    void media(int [] a){
        
        double media = 0;
        
        for(int i = 0; i < a.length;i++){
            media += a[i];
        }
        
        System.out.println("Media dos valores: " + media/a.length);
    }
    
    
    void mediana(int [] a){
        
        Arrays.sort( a );
        
        double medi;
        
        if(a.length % 2 == 0){
            
            medi = (a[(a.length/2) - 1] + a[a.length/2])/2;
            
            System.out.println("Mediana: " + medi);
            
        }
        else{
            
            medi = a[(a.length / 2)];
            System.out.println("Mediana: " + medi);
        }
    }
    
    void moda(int [] a){
        
        Arrays.sort( a );
        
        int [] tabela = new int[a[a.length - 1]+1];
        
        for(int i = 0; i < a.length; i++){
            tabela[a[i]]++;
        }
        
        int aux = tabela[0];
        int p = 0;
        for(int i = 0; i < tabela.length;i++){
            
            if(tabela[i] > aux){
                aux = tabela[i];
                p = i;
            }
        }
        
        System.out.println("Moda: " + aux);
    }
}

/*Escreva uma classe Estatística em Java que contenha
métodos que recebam um array de inteiros e calculem:
a) a moda dos elementos no array (elemento mais frequente).
b) A mediana dos elementos no array (elemento central).
c) a média.
*/
package questaoestatistica;

import java.util.Scanner;

public class QuestaoEstatistica {

    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        Estatistica es = new Estatistica();
        
        int [] n = new int[1];
        
        
        
        for(int i = 0;i < n.length;i++){
    
            
            
            System.out.println("Digite um valor inteiro: ");
            
            n[i] = teclado.nextInt();
            
            teclado.nextLine();
            
            System.out.println("Deseja continuar?[sim/nao]");
            
            if("sim".equals(teclado.nextLine())){
                
                int [] temp = new int[n.length];   
                
                System.arraycopy(n, 0, temp, 0, n.length);     
               
                n = new int[n.length+1];
                
                System.arraycopy(temp, 0, n, 0, temp.length);
             
            }
            else{
                break;
            }
        }
        
        es.media(n);
        es.mediana(n);
        es.moda(n);
    }
    
}

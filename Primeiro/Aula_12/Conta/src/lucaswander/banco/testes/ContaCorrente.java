/*Crie duas subclasses da classe Conta: ContaCorrente e
ContaPoupanca. Ambas terão o método atualiza reescrito: A
ContaCorrente deve atualizar-se com o dobro da taxa e a
ContaPoupanca deve atualizar-se com o triplo da taxa. Além
disso, a ContaCorrente deve reescrever o método deposita, a
fim de retirar uma taxa bancária de dez centavos de cada
depósito.
*/
package lucaswander.banco.testes;


public class ContaCorrente extends Conta implements Tributavel{
    
    
    SeguroDeVida seg;
    
    @Override
    public void atualiza(double taxa){
        this.saldo += this.saldo * (2*taxa);
    }
    
    @Override
    public void deposita(double valor){
        this.saldo += valor - 10;
    }

    @Override
    public double calculaTributos() {
        
        return (this.saldo * 0.01) + 42;
    }
}

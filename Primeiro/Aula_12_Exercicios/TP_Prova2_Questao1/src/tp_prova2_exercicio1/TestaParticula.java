/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_prova2_exercicio1;

/**
 *
 * @author Virginia
 */
public class TestaParticula {
    
    public static void main(String args[]){
        StdDraw.setXscale(-10.0,10.0);
        StdDraw.setYscale(-10.0,10.0);
        
        Particula p1 = new Particula(-10.0,-10.0,0.01,4.5,1,0.75);
        Particula p2 = new Particula(0.0,10.0,0.01,1.5,0.75,1);
        while(true){
            StdDraw.clear();
            StdDraw.setPenColor(StdDraw.RED);
            p1.mover();
            StdDraw.setPenColor(StdDraw.BLUE);
            p2.mover();
            p1.verificarColisao(p2);
            StdDraw.show(20);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_prova2_exercicio1;

/**
 *
 * @author Virginia
 */
public class Particula extends Ponto{
    private double massa;
    private double vx;
    private double vy;
    
    public Particula(){
        this(0.0,0.0,0.01,0.5,1,0.75);
    }
    
    public Particula(double a, double b, double c, double d, double e, double f){
        super (a,b,c);
        setMassa(d); 
        setVx(e); 
        setVy(f);
    }
 
    public void setMassa(double d) {
        massa = (d > 0)?d:0;
        setRaio(Math.sqrt(massa/Math.PI));
    }

    public void setVx(double e){
        vx = e;
    }
    
    public void setVy(double f){
        vy = f;
    }

    public double getMassa(){
        return massa;
    }

    public double getVx(){
        return vx;
    }

    public double getVy(){
        return vy;
    }
    
    @Override
    public void desenhar(){
        StdDraw.filledCircle(getX(),getY(),getRaio());
    }

    public void mover(){ 
    // Realiza a movimentação da partícula observando a parede !
        verificarLimite();
        setX(getX()+vx);
        setY(getY()+vy);
        desenhar();
    }
    
    public void verificarLimite(){ // Verifica se a partícula colidiu com a parede !
        double aux1 = getX()+vx+getRaio();
        double aux2 = getY()+vy+getRaio(); // Se houver colisão, então, mudar a direção da velocidade !
        if ((aux1 > 10.0)||(aux1 < -10.0)){
            vx = -vx;
        }
        if ((aux2 > 10.0)||(aux2 < -10.0)){
            vy = -vy;}
    }
    
    public void verificarColisao(Particula p){ // Calculando a distância entre os centros de duas partículas !
        double dx = Math.pow((p.getX()-(getX())),2);
        double dy = Math.pow((p.getY()-(getY())),2);
        double d = Math.sqrt(dx + dy); // Calculando a distância entre os centros de 2 partículas !
        double r = p.getRaio() + getRaio(); // Ocorreu uma colisão !!
        if (d <= r){
            setVx(-(1.0*getVx()));
            setVy(-(1.0*getVy()));
            p.setVx(-(1.0*getVx()));
            p.setVy(-(1.0*getVy()));}
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tp_prova2_exercicio1;

/**
 *
 * @author Virginia
 */
public class Ponto {
    
    private double x;
    private double y;
    private double raio ;

    public Ponto(){
        this(0.0, 0.0, 0.01);
    }

    public Ponto(double a, double b, double c){
        setX(a); setY(b); setRaio(c);
    }
    
    public void setX(double a){
        x = a;
    }

    public void setY(double b){ 
        y = b; 
    }
    
    public void setRaio(double c){ 
        raio = (c > 0)?c:0.01;
    }
    
    public double getRaio(){
        return raio;
    }
    
    public double getX(){
        return x;
    }
    
    public double getY(){
        return y;
    }
    
    public void desenhar(){
        StdDraw.filledCircle(x, y, raio); 
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questao1;

/**
 *
 * @author strudel
 */
public class Circulo implements AreaCalculavel{

    double raio;
    
    public double calculaArea() {
        
        return 3.14 * this.raio * this.raio;
    }
    public void setRaio(double v){
        this.raio = v;
    }
    
    public double getRaio(){
        return this.raio;
    }
    
    
}

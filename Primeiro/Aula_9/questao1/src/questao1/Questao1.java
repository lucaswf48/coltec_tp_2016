
package questao1;


public class Questao1 {


    public static void main(String[] args) {
        
        
        Circulo a1 = new Circulo();
        Retangulo a2 = new Retangulo();
        Quadrado a3 = new Quadrado();
        
        a1.setRaio(1);
        a2.setAltura(1);
        a2.setBase(2);
        a3.setAltura(1);
        
        AreaCalculavel teste = a1;
        
        System.out.println("Circulo: " + teste.calculaArea());
        
        teste = a2;
        
        System.out.println("Retangulo: " + teste.calculaArea());
        
        teste = a3;
        
        System.out.println("Quadrado: " + teste.calculaArea());
        
    }
    
}

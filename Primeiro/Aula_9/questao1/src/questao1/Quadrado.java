
package questao1;


public class Quadrado implements AreaCalculavel{

    double lado;
    
    public double calculaArea() {
        
        return this.lado * this.lado;
    }
    public void setAltura(double v){
        this.lado = v;
    }
    
    public double getAltura(){
        return this.lado;
    }
}

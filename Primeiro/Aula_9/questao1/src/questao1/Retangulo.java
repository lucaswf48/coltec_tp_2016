/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questao1;

/**
 *
 * @author strudel
 */
public class Retangulo implements AreaCalculavel{

    private double altura;
    private double base;
    
    public double calculaArea() {
        
        return this.altura * this.base;
    }
    
    public void setAltura(double v){
        this.altura = v;
    }
    
    public double getAltura(){
        return this.altura;
    }
    
    public void setBase(double v){
        this.base = v;
    }
    
    public double getBase(){
        return this.base;
    }
    
}

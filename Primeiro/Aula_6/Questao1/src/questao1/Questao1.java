/*

1. Adicione o modificador de visibilidade (private, se
necessário) para cada atributo e método da classe Funcionario.
Tente criar um Funcionario no main e modificar ou ler um de
seus atributos privados. O que acontece? 

R.Ocorre um erro de compilação.

7. Como garantir que datas como 31/2/2012 não sejam
aceitas pela sua classe Data?

R. Fazer um metodo setData que fique encarregado de validar datas

*/



package questao1;

public class Questao1 {

   
    public static void main(String[] args) {
        
        
        Empresa emp = new Empresa();
        
        for(int i = 0; i < 10; i++){
            emp.adicionarFuncionario(new Funcionario());
        }
        
        emp.mostraEmpregados();
    }
    
}


package questao1;


public class Funcionario {
    
    private static int numeroDeFuncionarios;
    private String nome;
    private String departamento;
    private String rg;
    private Data dataDeEntrada = new Data();
    private double salario;
    private int identificador;
    
    Funcionario(){
        Funcionario.numeroDeFuncionarios++;
        this.identificador = Funcionario.numeroDeFuncionarios;
    }
    
    Funcionario(String n){
        this();
        this.nome = n;
    }
    
    public int getIdentificador(){
        return this.identificador;
    }
    
    public String getNome(){
        return this.nome;
    }
    
    public void setNome(String n){
        this.nome = n;
    }
    
    public String getDepartamento(){
        return this.departamento;
    }
    
    public void setDepartamento(String d){
        this.departamento = d;
    }
    
    public String getRg(){
        return this.rg;
    }
    
    public void setRg(String r){
        this.rg = r;
    }
    
    public String getData(){
        return this.dataDeEntrada.dataFormatada();
    }
    
    public void setData(int dia, int mes, int ano){
        this.dataDeEntrada.setData(dia, mes, ano);
    }
    
    public double getSalario(){
        
        return this.salario;
    }
    
    public void setSalario(double s){
        this.salario = s;
    }
    
    public void mostra(){
        
        System.out.println("Nome: " + this.nome);
        System.out.println("Departamento: " + this.departamento);
        System.out.println("RG: " + this.rg);
        System.out.println("Data de entrada: " + this.dataDeEntrada.dataFormatada());
        System.out.println("Salario: " + this.salario);
    }
}

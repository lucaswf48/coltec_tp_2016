
package questao1;


public class Data {

    
    private int dia;
    private int mes;
    private int ano;
    
    public void setData(int d, int m, int a){
        this.dia = d;
        this.mes = m;
        this.ano = a;               
    }
    
    public String dataFormatada() {
        
        return dia + "/" + mes + "/"+ ano;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.Scanner;
import javax.swing.JFileChooser;


/**
 *
 * @author lucas
 */
public class RelatorioCampeonato {
    
    
    
    
    public void situacaoAtual(Campeonato c) throws FileNotFoundException{
        
        Scanner s = new Scanner(System.in);
        
        JFileChooser file = new JFileChooser () ;
        file.showOpenDialog(null);
        
        File fi = file.getSelectedFile();
        
        PrintStream ps1 = new PrintStream(fi);
        
        

        
       
        ps1.println(c.getNome() + " " + c.getAnoDeRealizacao());
        
        Time []t = c.getTimes();
        
        
        for(int i = 0; i < t.length; ++i){
            ps1.println(t[i].getNome() + " " + t[i].getTecnico().getNome());
            
            for(int j = 0; j < t[i].getJogadores().length;++j){
               ps1.println(t[i].getJogadores()[j].getNome() + "  " +  t[i].getJogadores()[j].getPosicao());
            }
            
            ps1.println();
        }
        
        String[] nomes = new String[t.length];
        int[] pontos = new int[t.length];
        
        for(int i = 0; i < t.length; ++i){
            pontos[i] = t[i].getPontos();
            nomes[i] = t[i].getNome();
        }
        

        int aux;
       for(int i=pontos.length-1; i >= 1; i--) 
       {
          
        for( int j=0; j < i ; j++) 
        {
            if(pontos[j]>pontos[j+1]) 
            {
                aux = pontos[j];
                String aux2 = nomes[j];
                nomes[j] = nomes[j + 1];
                pontos[j] = pontos[j+1];
                nomes[j + 1] = aux2;
                pontos[j+1] = aux;
            }
        }
    }
        
    
        
        for(int i = t.length - 1; i >= 0; --i){
            ps1.println(nomes[i] + " " + pontos[i]);
        }
        
    }
}

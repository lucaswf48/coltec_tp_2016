/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

/**
 *
 * @author lucas
 */
public class Jogador extends Pessoa{
    
    //posição que joga e time.
    private String posicao;
    

    public Jogador(){
        
    }

    public Jogador(String posicao,String n) {
        this.posicao = posicao;
        this.nome = n;
    }
    
    public void setTime(Time t){
        this.time = t;
    }
    
    /**
     * @return the posicao
     */
    public String getPosicao() {
        return posicao;
    }

    /**
     * @param posicao the posicao to set
     */
    public void setPosicao(String posicao) {
        this.posicao = posicao;
    }

    /**
     * @return the time
     */
   
    
}

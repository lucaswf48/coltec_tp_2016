/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

import java.io.FileNotFoundException;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class TestaCampeonato {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException {
        // TODO code application logic here
        
        
    
        
        
        Campeonato mineiro = new Campeonato(2016,"Campeonato Mineiro");
        Time atletico = new Time("Atletico");
        Time cruzeiro = new Time("Cruzeiro");
        RelatorioCampeonato relatorio = new RelatorioCampeonato(); 
        

        Jogador jogador1 = new Jogador("Goleiro","a");
        Jogador jogador2 = new Jogador("Zagueiro","b");
        Jogador jogador3 = new Jogador("Atacante","c");
        Jogador jogador4 = new Jogador("Zagueiro","d");
        Jogador jogador5 = new Jogador("Goleiro","e");
        Jogador jogador6 = new Jogador("Goleiro","f");
        
        Tecnico tecnicoAtletico = new Tecnico("123", atletico,"lucas");
        Tecnico tecnicoCruzeiro = new Tecnico("123", cruzeiro,"Wander");
        
        atletico.setTecnico(tecnicoAtletico);
        cruzeiro.setTecnico(tecnicoCruzeiro);
        
        tecnicoCruzeiro.adicionaJogador(jogador1);
        tecnicoCruzeiro.adicionaJogador(jogador2);
        tecnicoCruzeiro.adicionaJogador(jogador6);
               
        tecnicoAtletico.adicionaJogador(jogador3);
        tecnicoAtletico.adicionaJogador(jogador4);
        tecnicoAtletico.adicionaJogador(jogador5);
        tecnicoAtletico.adicionaJogador(jogador1);
        
                
        
        mineiro.adicionaTime(atletico);
        mineiro.adicionaTime(cruzeiro);
        
        atletico.setNumeroDeDerrotas(0);
        atletico.setNumeroDeEmpates(1);
        atletico.setNumeroDeVitorias(2);
        
        cruzeiro.setNumeroDeDerrotas(3);
        cruzeiro.setNumeroDeEmpates(0);
        cruzeiro.setNumeroDeVitorias(0);
        
        
        relatorio.situacaoAtual(mineiro);
    }
   
}

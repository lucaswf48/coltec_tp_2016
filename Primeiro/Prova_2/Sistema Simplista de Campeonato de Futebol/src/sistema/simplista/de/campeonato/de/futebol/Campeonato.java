/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

/**
 *
 * @author lucas
 */
public class Campeonato {
    
    private String nome;
    private int anoDeRealizacao;
    private Time[] times = new Time[0];
    
    public Campeonato() {
    }
    
    
    public Campeonato(int a, String n) {
        
        
        this.setNome(n);
        this.setAnoDeRealizacao(a);
    }
    
    
    
    
    public void adicionaTime(Time t){
        
        Time[] aux = new Time[this.times.length + 1];
        for(int i = 0; i < this.times.length; ++i){
            aux[i] = this.times[i];
        }
        
        aux[this.times.length] = t;
        
        this.times = aux;
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the data
     */
    public int getAnoDeRealizacao() {
        return anoDeRealizacao;
    }

    /**
     * @param data the data to set
     */
    public void setAnoDeRealizacao(int data) {
        this.anoDeRealizacao = data;
    }
    /**
     * @return the times
     */
    public Time[] getTimes() {
        return times;
    }
    
    
}

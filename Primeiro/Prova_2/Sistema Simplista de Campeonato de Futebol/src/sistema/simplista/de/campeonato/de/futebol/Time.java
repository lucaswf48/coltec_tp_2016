/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

/**
 *
 * @author lucas
 */

//Cada time possui um nome, número de vitórias, número de derrotas, número de empates,
//e é formado por 22 jogadores e 1 técnico.

public class Time {
    
    private String nome;
    
    private int numeroDeVitorias;
    private int numeroDeDerrotas;
    private int numeroDeEmpates;
    
    
    private Jogador[] jogadores = new Jogador[0];
    private Tecnico tecnico;

    public Time(String n) {
        this.nome = n;
    }

    public int  getPontos(){
        
        return (this.numeroDeEmpates * 1) + (this.numeroDeVitorias * 3);
    }
    
    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the numeroDeVitorias
     */
    public int getNumeroDeVitorias() {
        return numeroDeVitorias;
    }

    /**
     * @param numeroDeVitorias the numeroDeVitorias to set
     */
    public void setNumeroDeVitorias(int numeroDeVitorias) {
        this.numeroDeVitorias = numeroDeVitorias;
    }

    /**
     * @return the numeroDeDerrotas
     */
    public int getNumeroDeDerrotas() {
        return numeroDeDerrotas;
    }

    /**
     * @param numeroDeDerrotas the numeroDeDerrotas to set
     */
    public void setNumeroDeDerrotas(int numeroDeDerrotas) {
        this.numeroDeDerrotas = numeroDeDerrotas;
    }

    /**
     * @return the numeroDeEmpates
     */
    public int getNumeroDeEmpates() {
        return numeroDeEmpates;
    }

    /**
     * @param numeroDeEmpates the numeroDeEmpates to set
     */
    public void setNumeroDeEmpates(int numeroDeEmpates) {
        this.numeroDeEmpates = numeroDeEmpates;
    }
    
    
    
    /**
     * @return the jogadores
     */
    public Jogador[] getJogadores() {
        return jogadores;
    }

    /**
     * @param jogadores the jogadores to set
     */
    public void setJogadores(Jogador[] jogadores) {
        this.jogadores = jogadores;
    }

    /**
     * @return the tecnico
     */
    public Tecnico getTecnico() {
        return tecnico;
    }

    /**
     * @param tecnico the tecnico to set
     */
    public void setTecnico(Tecnico tecnico) {
        this.tecnico = tecnico;
    }

    private boolean verificaPosicoes(Jogador j){
   
        for(int i = 0; i < this.jogadores.length; ++i){
            if(this.jogadores[i] != null){
                if(j.getPosicao().equals(this.jogadores[i].getPosicao()))
                    return true;
            }
        }
        
        return false;
    }
    
    void adicionaJogador(Jogador j) {
        
        
        if(j.time != null){
            throw new JogadorComDoisTImesException();
        }
        
        
        if(this.verificaPosicoes(j)){
            throw new PosicaoRepitidaException();
        }
        
        Jogador[] aux = new Jogador[this.jogadores.length + 1];
            
        for(int i = 0; i < this.jogadores.length; ++i){
            aux[i] = this.jogadores[i];
        }

        aux[this.jogadores.length] = j;

        this.jogadores = aux;
    }
    
    
}

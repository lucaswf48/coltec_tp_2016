/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistema.simplista.de.campeonato.de.futebol;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class Tecnico extends Pessoa{
    
    private String senha;

    public Tecnico(String senha,Time t,String n) {
        
        this.nome = n;
        this.time = t;
        this.senha = senha;
    }
    
    public String getSenha(){
        return this.senha;
    }
    
    private void autenticaTecnico(String s){
        
        
        if(this.senha.equals(s)){
            
        }
        else{
             throw new SenhaInvalidaException();
        }
    }

    void adicionaJogador(Jogador j){
        
        Scanner tecla = new Scanner(System.in);
        
        try{
            System.out.println("Digite sua senha: ");
            this.autenticaTecnico(tecla.nextLine());
            this.time.adicionaJogador(j);
            j.setTime(this.time);
        }
        catch(SenhaInvalidaException s){
            JOptionPane .showMessageDialog(null, s.getMenssagem() + " \n" + "Jogador: " + j.getNome(), "Erro " , JOptionPane . ERROR_MESSAGE ) ;
            
        }
        catch(PosicaoRepitidaException s){
            JOptionPane .showMessageDialog(null, s.getMenssagem() +  " \n" + "Jogador: " + j.getNome(), "Erro " , JOptionPane . ERROR_MESSAGE ) ;
            
        }
        catch(JogadorComDoisTImesException s){
            JOptionPane .showMessageDialog(null, s.getMenssagem() + " \n" + "Jogador: " + j.getNome(), "Erro " , JOptionPane . ERROR_MESSAGE ) ;
            
        }
        
    }
}


package questao8;

import java.util.Scanner;


public class Questao8 {

 
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Digite o numero de linas:");
        int n = entrada.nextInt();
        
        for(int i = 1; i <= n; i++){
            
            for(int j = 1; j <= i; j++){
                System.out.print(j * i + " ");
            }
            System.out.println("");
        }
    }
    
}

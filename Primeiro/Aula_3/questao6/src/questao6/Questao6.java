
package questao6;


public class Questao6 {


    public static void main(String[] args) {
        
        int fibo = 1,
            anterior = 0,
            atual = 1;
        
  
            
            while(fibo<=100){
                fibo=atual+anterior;
                if(fibo > 100)
                    break;
                System.out.print(fibo+",");
                anterior=atual;
                atual=fibo;
        }
    
    }
}

package questao7;

import java.util.Scanner;


public class Questao7 {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        
        System.out.println("Digite o valor de x:");
        int x = entrada.nextInt();
        
        do{
            if(x % 2 == 0)
                x /= 2;
            else
                x = 3 * x + 1;
            
            System.out.println("Valor de x " + x);
        }while(x != 1);
    }
    
}

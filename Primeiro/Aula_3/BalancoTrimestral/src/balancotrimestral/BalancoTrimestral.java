
package balancotrimestral;

/**
 *
 * @author Lucas
 */
public class BalancoTrimestral {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        int gastosJaneiro = 15000,
            gastosFevereiro = 23000,
            gastosMarco = 17000,
            gastosTrimestre = gastosJaneiro + gastosFevereiro + gastosMarco;
        
        double mediaMensal = gastosTrimestre / 3;
        
        System.out.println("O gasto trimestral foi de " + gastosTrimestre + " reais.");
        System.out.println("Valor da média mensal = " + mediaMensal + " reais.");
        
        
    }
    
}

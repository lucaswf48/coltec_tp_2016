
package questao4;

public class Questao4 {

    public static void main(String[] args) {
     
        
        //Fatorial de um numero maior que 20 ocorre overflow(grande demais para o computador representar)
        for(int i = 1; i <= 20; i++){
            
            long fatorial = 1;
            
            for(int j = 1; j <= i; j++){
                fatorial *= j;
                
            }
            System.out.println("Fatorial de " + i + " é " + fatorial);
            
        }
    }
    
}

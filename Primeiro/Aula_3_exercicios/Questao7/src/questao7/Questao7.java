
package questao7;

import java.util.Scanner;


public class Questao7 {

    
    public static void main(String[] args) {
        
        
        Scanner teclado = new Scanner(System.in);
        
        int idade,
            totalHomens = 0,
            totalMulheres = 0;
        
        String sexo,m = "M",h = "H";
        
        double mediaIdadeHomemens = 0,
               mediaPesoMulheres = 0,
               peso;
        
        for(int i = 1; i <= 10;i++){
            
            System.out.println("Digite o seu sexo:(H-homem/M-mulher)");
            sexo = teclado.nextLine();
            System.out.println("Digite sua idade:");
            idade = teclado.nextInt();
            System.out.println("Digite seu peso:");
            peso = teclado.nextDouble();
            teclado.nextLine();
            
            if(m == "M"){
                totalMulheres++;
                mediaPesoMulheres += peso;
            }
            else if(h == "H"){
                totalHomens++;
                mediaIdadeHomemens += idade;
            }
            else{
                System.out.println("Sexo indefinido");
                i -= 2;
            }
            
        }
        
        System.out.println("Total de mulheres: " + totalMulheres + "\nMedia de peso:" + mediaPesoMulheres/totalMulheres);
        System.out.println("Total de homens: " + totalMulheres + "\nMedia de idade:" + mediaPesoMulheres/totalHomens);
    }
    
}


package questao1;

import java.util.Scanner;


public class Questao1 {

  
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        int anoNascimento,anoAtual;
        
        System.out.println("Qual é o seu ano de nascimento?");
        anoNascimento = teclado.nextInt();
        
        System.out.println("Qual é o ano atual?");
        anoAtual = teclado.nextInt();
        
        System.out.println("Idade: " + (anoAtual - anoNascimento));
        
        
        
    }
    
}

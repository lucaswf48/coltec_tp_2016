
package questao5;

import java.util.Scanner;


public class Questao5 {

    
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        char combustivel;
        int litros;
        double valorPago;
        
        System.out.println("Qual combustivel foi abastecido(A-álcool/G-gasolina):");
        
        combustivel = teclado.nextLine().charAt(0);
        
        System.out.println("Quantos litros foram abastecidos?");
        
        litros = teclado.nextInt();
        
        
        
        
        if(combustivel == 'G' || combustivel == 'A'){
            
            if(litros <= 20 && combustivel == 'G'){
                valorPago = litros * 0.96 * 2.99;
            }
            else if(litros > 20 && combustivel == 'G'){
                valorPago = litros * 0.94 * 2.99;
            }
            else if(litros > 20 && combustivel == 'A'){
                valorPago = litros * 0.97 * 2.50;
            }
            else{
                valorPago = litros * 0.95 * 2.50;
            }
            
            System.out.println("Valor a ser pago: " + valorPago + " reais.");
        }
        
        
        
    }
    
}

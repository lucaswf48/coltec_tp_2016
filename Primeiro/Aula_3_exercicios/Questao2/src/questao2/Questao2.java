
package questao2;

import java.util.Scanner;


public class Questao2 {

  
    public static void main(String[] args) {
        
        double media = 0;
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Digite o valor da primeira nota:");
        media += teclado.nextInt();
        System.out.println("Digite o valor da segunda nota:");
        media += teclado.nextInt();
        System.out.println("Digite o valor da terceira nota:");
        media += teclado.nextInt();
        
        media /= 3;
        if(media >= 7.0){
            System.out.println("Aprovado");
        }
        else if(media >= 5.0){
            System.out.println("Recuperação");
        }
        else{
            System.out.println("Reprovado");
        }
        
    }
    
}

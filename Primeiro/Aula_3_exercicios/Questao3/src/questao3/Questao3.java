
package questao3;

import java.util.Scanner;

public class Questao3 {

    
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        int ano;
        
        System.out.println("Entre com um ano com 4 digitos:");
        ano = teclado.nextInt();
        if(ano % 400 == 0){
            System.out.println("Ano bissexto");
        }
        else if(ano % 4 == 0 && ano % 100 != 0){
            System.out.println("Ano bissexto");
        }
        else{
            System.out.println("Não é um ano bissexto");
        }
    }
    
}

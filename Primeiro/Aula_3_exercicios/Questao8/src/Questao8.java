
import java.util.Scanner;


public class Questao8 {


    public static void main(String[] args) {
       
        int voto,
            nulo = 0,
            branco = 0,
            totalVotos = 0,
            c1 = 0,
            c2 = 0,
            c3 = 0,
            c4 = 0;
        
        double pNulos,
               pBrancos;
        
        Scanner teclado = new Scanner(System.in);
        
        do{
            System.out.println("VOTE");
            System.out.println("1 - c1");
            System.out.println("2 - c2");
            System.out.println("3 - c3");
            System.out.println("4 - c4");
            System.out.println("5 - Nulo");
            System.out.println("6 - Branco");
            System.out.println("0 - Sair");
            voto = teclado.nextInt();
            switch(voto){
                case 0:
                    break;
                case 1:
                    c1++;
                    break;
                case 2:
                    c2++;
                    break;
                case 3:
                    c3++;
                    break;
                case 4:
                    c4++;
                    break;
                case 5:
                    nulo++;
                    break;
                case 6:
                    branco++;
                    break;
                default:
                    totalVotos--;
                    System.out.println("Opção invalida");
            }
            
            totalVotos++;
        }while(voto != 0);
        
        System.out.println("Candidato 1: " + c1);
        System.out.println("Candidato 2: " + c2);
        System.out.println("Candidato 3: " + c3);
        System.out.println("Candidato 4: " + c4);
        
        System.out.println("Total de votos nulos: " + nulo);
        System.out.println("Total de votos nulos: " + branco);
        System.out.println("Porcentagem de votos nulos: " + ((nulo*100)/totalVotos));
        System.out.println("Porcentagem de votos em branco: " +((branco*100)/totalVotos));
        
    }
    
}

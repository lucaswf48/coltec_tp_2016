/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package questao4;

import java.util.Scanner;

/**
 *
 * @author Lucas
 */
public class Questao4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner teclado = new Scanner(System.in);
        
        int resposta;
        int culpa = 0;
        
        System.out.println("Responta estas 5 perguntas:");
        System.out.println("Telefonou para a vítima?(sim - 1/não - 0)");
        resposta = teclado.nextInt();
        if(resposta == 1){
            culpa++;
        }
        System.out.println("Esteve no local do crime?(sim - 1/não - 0)");
        resposta = teclado.nextInt();
        if(resposta == 1){
            culpa++;
        }
        System.out.println("Mora perto da vítima?(sim - 1/não - 0)");
        resposta = teclado.nextInt();
        if(resposta == 1){
            culpa++;
        }
        System.out.println("Devia para a vítima?(sim - 1/não - 0)");
        resposta = teclado.nextInt();
        if(resposta == 1){
            culpa++;
        }
        System.out.println("Já trabalhou com a vítima?(sim - 1/não - 0)");
        resposta = teclado.nextInt();
        if(resposta == 1){
            culpa++;
        }
     
        switch(culpa){
            case 2:
                System.out.println("Suspeita");
                break;
            case 3:
            case 4:
                System.out.println("Cúmplice");
                break;
            case 5:
                System.out.println("Assassino");
                break;
            default:
                System.out.println("Inocente");
                break;
        }
        
    }
    
}


package questao6;

import java.util.Scanner;


public class Questao6 {

  
    public static void main(String[] args) {
        
        Scanner teclado = new Scanner(System.in);
        
        System.out.println("Digite um numero inteiro:");
        
        int numero = teclado.nextInt();
        
        for(int i = 2; i < (numero/2 + 1); i++){
            if(numero % i == 0){
                System.out.println("O numero " + numero + " não é primo.");
                return;
            }
        }
        
        System.out.println("O numero " + numero + " é primo.");
    }
    
}

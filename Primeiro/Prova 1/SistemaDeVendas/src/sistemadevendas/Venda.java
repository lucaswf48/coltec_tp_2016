/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class Venda {
    
    private static long numeroDeVendas = 0;
    private long codVenda;
    private String dataVenda;
    private long codCliente;
    private Cliente cliente;
    private DetalheVenda detalhe;
    
            
    Venda(){
        
        this.codVenda = numeroDeVendas;
        numeroDeVendas++;
    }
    
    public boolean conclueVenda(){
       
        return this.detalhe.conclueVenda();
    }
    
    public void getInformacoes(){
        System.out.println("Codigo de venda: " + this.codVenda);
        System.out.println("Data de venda: "  + this.dataVenda);
        System.out.println("Codigo do cliente: "  + this.codCliente);
        System.out.println("Valor total: " + this.getValorTotal());
    }
    
    public double getValorTotal(){
        
        return this.detalhe.getSubTotal();
    }
    
    public long getCodVenda() {
        return codVenda;
    }

    public String getDataVenda() {
        return dataVenda;
    }

    public void setDataVenda(String dataVenda) {
        this.dataVenda = dataVenda;
    }

    public long getCodCliente() {
        return codCliente;
    }

    public void setCodCliente(long codCliente) {
        this.codCliente = codCliente;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public DetalheVenda getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(DetalheVenda detalhe) {
        this.detalhe = detalhe;
    }
       
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class PessoaJuridica extends Cliente{
    
    private String cnpj;
    private String[] filiais = new String[0];
    
    public String getCnpj() {
        
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        if(this.utilitario.validaCpf(cnpj))
            this.cnpj = cnpj;
        else{
            System.out.println("Cnpj invalido.");
        }
    }

    public String[] getFiliais() {
        return filiais;
    }

    public void setFiliais(String[] filiais) {
        this.filiais = filiais;
    }
    
    
}

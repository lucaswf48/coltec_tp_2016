/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class TesteVendas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        PessoaFisica c1 = new PessoaFisica();
        PessoaJuridica c2 = new PessoaJuridica();
        Venda v1 = new Venda();
        
        DetalheVenda d1 = new DetalheVenda();
        Produto p1 = new Produto();
        
        p1.setDetalhe(d1);
        p1.setValorUnitario(500);
        p1.setQtdEstoque(10);
        p1.setEstoqueMinimo(2);
        
        System.out.println("\n----------------------------------------\n");
        System.out.println("Produto");
        p1.getInformacoes();
        
        
        c1.setNomeCliente("Lucas Wander de Freitas");
        c1.setEmail("lucaswf48@gmail.com");
        c1.setCpf("14030979688");
        c1.setRenda(5000);
        c1.setEndereco("rua daw");
        
        System.out.println("\n----------------------------------------\n");
        System.out.println("Cliente:");
        c1.getInformacoes();
        
        
        d1.setVenda(1);
        d1.setProduto(p1);
        d1.setQtdProduto(3);
        
        System.out.println("\n----------------------------------------\n");
        System.out.println("Detalhes da venda:");
        d1.getInformacoes();
        
        
        v1.setCliente(c1);
        v1.setCodCliente(c1.getCodCliente());
        v1.setDetalhe(d1);
        
        System.out.println("\n----------------------------------------\n");
        System.out.println("Venda: ");
        v1.getInformacoes();
        
      
        v1.conclueVenda();       
        p1.subirEstoque(3);
        
        
        System.out.println("\n----------------------------------------\n");
        System.out.println("Produto");
        p1.getInformacoes();
    }
    
}


package sistemadevendas;

public class Cliente {
    
    protected  static long numeroDeClientes = 0;
    
    protected Utilitario utilitario = new Utilitario();
    protected long codCliente;
    protected String nomeCliente;
    protected String email;
    protected double renda;
    protected String classe;
    protected Venda[] vendas = new Venda[0];
    
    
    Cliente(){
        
        this.codCliente = numeroDeClientes;
        numeroDeClientes++;
    }
    
   
    
    private void setClasse(){
        
        if(this.renda < 2000){
            this.classe = "c";
            
        }
        else if(this.renda > 2000 && this.renda < 4000){
            this.classe = "b";
        }
        else{
            this.classe = "a";
        }
    }
    
    public void adicionaVenda(Venda v){
        
        Venda[] aux = new Venda[this.vendas.length + 1];
        
        System.arraycopy(this.vendas, 0, aux, 0, this.vendas.length);
        
        aux[this.vendas.length] = v;
        
        this.vendas = aux;
    }
    
    public long getCodCliente() {
        return codCliente;
    }

    public String getNomeCliente() {
        return nomeCliente;
    }

    public void setNomeCliente(String nomeCliente) {
        this.nomeCliente = nomeCliente;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        
        if(this.utilitario.validaEmail(email)){
            this.email = email;
        }
        else{
            System.out.println("Email invalido.");
        }
    }

    public double getRenda() {
        return renda;
    }

    public void setRenda(double renda) {
        this.renda = renda;
        setClasse();
    }

    public String getClasse() {
        return classe;
    }

   
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class DetalheVenda {
    
    private long venda;
    private Produto produto;
    private int qtdProduto;
    
    public void getInformacoes(){
        
        System.out.println("Quantidades: " + this.qtdProduto);
        System.out.println("SubTotal: " + this.getSubTotal());
    }
    
    
    public boolean conclueVenda(){
        
        if(this.produto.baixarEstoque(this.qtdProduto)){
            
            return true;
        }
        return false;
    }
    public double getSubTotal(){
        
        return this.qtdProduto * this.produto.getValorUnitario();
    }

    public long getVenda() {
        return venda;
    }

    public void setVenda(long venda) {
        this.venda = venda;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getQtdProduto() {
        return qtdProduto;
    }

    public void setQtdProduto(int qtdProduto) {
        this.qtdProduto = qtdProduto;
    }
    
    
}

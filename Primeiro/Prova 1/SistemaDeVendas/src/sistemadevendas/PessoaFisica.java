/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class PessoaFisica extends Cliente{
    
    
    
    private String cpf;
    private String endereco;

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        
        if(this.utilitario.validaCpf(cpf))
            this.cpf = cpf;
        else{
            System.out.println("Cpf invalido.");
        }
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public void getInformacoes() {
        
        System.out.println("Classe: " + this.getClasse());
        System.out.println("Cpf: " + this.getCpf());
        System.out.println("Email: " + this.getEmail());
        System.out.println("Renda: " + this.getRenda());
        System.out.println("Nome: " + this.getNomeCliente());
        System.out.println("Endereço: " + this.getEndereco());
    }
    
    
}

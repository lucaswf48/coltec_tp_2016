/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadevendas;

/**
 *
 * @author Lucas
 */
public class Produto {
    
    
    private long codProduto;
    private String unidade;
    private String descricao;
    private double valorUnitario;
    private int estoqueMinimo;
    private int qtdEstoque;
    private DetalheVenda detalhe;
    
    public void getInformacoes(){
        System.out.println("Valor unitario: " + this.unidade);
        System.out.println("Estoque minimo: " + this.estoqueMinimo);
        System.out.println("Quantidade em estoque: " + this.qtdEstoque);
        System.out.println("Descrição do produto: " + this.descricao);
    }
    
    public boolean baixarEstoque(int i){
        
        this.estoqueBaixo();
        if(this.qtdEstoque - i > this.estoqueMinimo){
            
            this.qtdEstoque -= detalhe.getQtdProduto();
            return true;
        }
        
        return false;
    }
    
    public boolean subirEstoque(int i){
        
        this.qtdEstoque += i;
        return true;
    }
    
    public boolean estoqueBaixo(){
        
        if(this.estoqueMinimo == this.qtdEstoque)
            return true;
        
       return false;
    }
    
    public long getCodProduto(){
        return codProduto;
    }

    public String getUnidade() {
        return unidade;
    }

    public void setUnidade(String unidade) {
        this.unidade = unidade;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValorUnitario() {
        return valorUnitario;
    }

    public void setValorUnitario(double valorUnitario) {
        this.valorUnitario = valorUnitario;
    }

    public int getEstoqueMinimo() {
        return estoqueMinimo;
    }

    public void setEstoqueMinimo(int estoqueMinimo) {
        this.estoqueMinimo = estoqueMinimo;
    }

    public int getQtdEstoque() {
        return qtdEstoque;
    }

    public void setQtdEstoque(int qtdEstoque) {
        this.qtdEstoque = qtdEstoque;
    }

    public DetalheVenda getDetalhe() {
        return detalhe;
    }

    public void setDetalhe(DetalheVenda detalhe) {
        this.detalhe = detalhe;
    }
}


package questao1;

public class Questao1 {

   
    public static void main(String[] args) {
        
        Funcionario f1 = new Funcionario();
        Funcionario f2 = new Funcionario();
        Funcionario f3 = f1;
        
        System.out.println(f1 == f2);
        
        System.out.println(f1 == f3);
        
        f1.mostra();
    }
    
}

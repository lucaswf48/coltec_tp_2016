
package questao1;


public class Funcionario {
    
    String nome;
    String departamento;
    String rg;
    Data dataDeEntrada = new Data();
    double salario;
    
    void mostra(){
        
        System.out.println("Nome: " + this.nome);
        System.out.println("Departamento: " + this.departamento);
        System.out.println("RG: " + this.rg);
        System.out.println("Data de entrada: " + this.dataDeEntrada.dataFormatada());
        System.out.println("Salario: " + this.salario);
    }
}

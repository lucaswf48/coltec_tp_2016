/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lucas
 */
public class TratamentoClass implements Runnable{

    Socket cliente;
    
    TratamentoClass(Socket cliente) {
        
        this.cliente = cliente;
    }

    @Override
    public void run() {
        
        MorseCodeConverter c1 = new MorseCodeConverter();
        
        Scanner s = null;
        try {
            s = new Scanner(cliente.getInputStream());
        } catch (IOException ex) {
            Logger.getLogger(TratamentoClass.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        while(s.hasNextLine()){
            
            String ss = s.nextLine();
            System.out.println(ss);
            System.out.println(c1.fromMorse(ss));
        }
        
        s.close();
        try {
            cliente.close();
        } catch (IOException ex) {
            Logger.getLogger(TratamentoClass.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}

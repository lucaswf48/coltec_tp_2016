package servidor;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author Virginia
 */
public class MorseCodeConverter {

    public static String fromMorse(String morseToString) {

        Map<String, Character> morse = new HashMap<>();
        morse.put(".-", 'a');
        morse.put("-...", 'b');
        morse.put("-.-.", 'c');
        morse.put("-..", 'd');
        morse.put(".", 'e');
        morse.put("..-.", 'f');
        morse.put("--.", 'g');
        morse.put("....", 'h');
        morse.put("..", 'i');
        morse.put(".---", 'j');
        morse.put("-.-", 'k');
        morse.put(".-..", 'l');
        morse.put("--", 'm');
        morse.put("-.", 'n');
        morse.put("---", 'o');
        morse.put(".--.", 'p');
        morse.put("--.-", 'q');
        morse.put(".-.", 'r');
        morse.put("...", 's');
        morse.put("-", 't');
        morse.put("..-", 'u');
        morse.put("...-", 'v');
        morse.put(".--", 'w');
        morse.put("-..-", 'x');
        morse.put("-.--", 'y');
        morse.put("--..", 'z');
        morse.put(".----", '1');
        morse.put("..---", '2');
        morse.put("...--", '3');
        morse.put("....-", '4');
        morse.put(".....", '5');
        morse.put("-....", '6');
        morse.put("--...", '7');
        morse.put("---..", '8');
        morse.put("----.", '9');
        morse.put("-----", '0');
        morse.put("|", ' ');

        String[] palavras = morseToString.split("   ");

        String result = "";

        for (String p : palavras) {

            String[] letras = p.split(" ");

            for (String l : letras) {
                result += morse.get(l);
            }
            
            result+=" ";

        }

        return result;
    }

    public static String toMorse(String stringToMorse) {

        String result = "";

        stringToMorse = stringToMorse.toLowerCase();

        int i = 0;

        while (i < stringToMorse.length()) {
            Map<Character, String> morse = new HashMap<>();
            morse.put('a', ".-");
            morse.put('b', "-...");
            morse.put('c', "-.-.");
            morse.put('d', "-..");
            morse.put('e', ".");
            morse.put('f', "..-.");
            morse.put('g', "--.");
            morse.put('h', "....");
            morse.put('i', "..");
            morse.put('j', ".---");
            morse.put('k', "-.-");
            morse.put('l', ".-..");
            morse.put('m', "--");
            morse.put('n', "-.");
            morse.put('o', "---");
            morse.put('p', ".--.");
            morse.put('q', "--.-");
            morse.put('r', ".-.");
            morse.put('s', "...");
            morse.put('t', "-");
            morse.put('u', "..-");
            morse.put('v', "...-");
            morse.put('w', ".--");
            morse.put('x', "-..-");
            morse.put('y', "-.--");
            morse.put('z', "--..");
            morse.put('1', ".----");
            morse.put('2', "..---");
            morse.put('3', "...--");
            morse.put('4', "....-");
            morse.put('5', ".....");
            morse.put('6', "-....");
            morse.put('7', "--...");
            morse.put('8', "---..");
            morse.put('9', "----.");
            morse.put('0', "-----");
            morse.put(' ', " ");
            
            String cod = morse.get(stringToMorse.charAt(i));
            
            if(cod != null) {
                result += cod + " ";
            }

            i++;
        }

        return result;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servidor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author lucas
 */
public class Servidor {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        
        try (ServerSocket servidor = new ServerSocket(12345)) {
            System.out.println("Porta 12345 aberta!");
            
            
            while(true){
                
                Socket cliente = servidor.accept();
                System.out.println("Ip: " + cliente.getInetAddress().getHostAddress());
                TratamentoClass tratamento = new TratamentoClass(cliente);
                
                Thread t = new Thread(tratamento);
                
                t.start();
            }
        }
        
    }
    
}

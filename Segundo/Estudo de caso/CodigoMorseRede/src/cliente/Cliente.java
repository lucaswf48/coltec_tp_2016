/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cliente;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Scanner;

/**
 *
 * @author Lucas
 */
public class Cliente {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws UnknownHostException, IOException{
        // TODO code application logic here
        
        MorseCodeConverter c1 = new MorseCodeConverter();
        Socket cliente = new Socket("127.0.0.1", 12345);
        
        System.out.println("O cliente se conectou ao servidor!");
        
        Scanner teclado = new Scanner(System.in);
        
        PrintStream saida = new PrintStream(cliente.getOutputStream());
        
        while(teclado.hasNextLine()){
            
            String s1 = c1.toMorse(teclado.nextLine());
            saida.println(s1);
        }
        
        saida.close();
        
        teclado.close();
        
        cliente.close();
    }
    
}

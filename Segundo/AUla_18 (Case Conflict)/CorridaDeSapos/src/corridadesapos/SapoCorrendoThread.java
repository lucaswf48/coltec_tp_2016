/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corridadesapos;

/**
 *
 * @author strudel
 */
public class SapoCorrendoThread extends Thread{
    
    
    String nome; // nome do sapo

    int distanciaCorrida = 0; // distância já corrida pelo sapo

    int distanciaTotalCorrida; // distância a ser corrida pelo sapo

    int pulo = 0; // pulo do sapo em cm

    int pulos = 0; // quantidades de pulos dados na corrida

    static int colocacao = 0; // colocação do sapo ao final da corrida

    final static int PULO_MAXIMO = 50; // pulo máximo em cm que um sapo pode dar
    
    public SapoCorrendoThread (String nome, int distanciaTotalCorrida) {

    /* chamando o construtor de Thread passando o nome do sapo como parâmetro */

        super(nome);

        this.distanciaTotalCorrida = distanciaTotalCorrida;

        this.nome = nome;

    }
    
    public void sapoImprimindoSituacao () {

        System.out.println("O " + nome + "pulou " + pulo + "cm \t e ja percorreu " + distanciaCorrida + "cm");

    }
    
    public void sapoPulando() {

        pulos++;

        pulo = (int) (Math.random() * PULO_MAXIMO);

        distanciaCorrida += pulo;

        if (distanciaCorrida > distanciaTotalCorrida) {

            distanciaCorrida = distanciaTotalCorrida;

        }

    }
    
    public void sapoDescansando () {

        yield();

    }
    
    public void colocacaoSapo(){
        colocacao++;
        System.out.println(nome + " foi o " + colocacao + " colocado com " + pulos + " pulos");
    }
    
    public void run(){
        while(distanciaCorrida < distanciaTotalCorrida){
            sapoPulando();
            sapoImprimindoSituacao();
            sapoDescansando();
        }
        
        colocacaoSapo();
    }
}

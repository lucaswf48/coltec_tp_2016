/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corrida;

/**
 *
 * @author Lucas
 */
public class Corrida {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        double distanciaDaCorrida = 1000;
        
        CorridaT daytona = new CorridaT(distanciaDaCorrida);
        
        Piloto p1 = new Piloto("Brad",distanciaDaCorrida);
        Piloto p2 = new Piloto("Kevin",distanciaDaCorrida);
        Piloto p3 = new Piloto("Khane",distanciaDaCorrida);
        Piloto p4 = new Piloto("Trevor",distanciaDaCorrida);
        Piloto p5 = new Piloto("Danica",distanciaDaCorrida);
        
        Piloto p6 = new Piloto("Denny",distanciaDaCorrida);
        Piloto p7 = new Piloto("Tony",distanciaDaCorrida);
        Piloto p8 = new Piloto("Greg",distanciaDaCorrida);
        Piloto p9 = new Piloto("Kyle",distanciaDaCorrida);
        Piloto p10 = new Piloto("Matt",distanciaDaCorrida);
        
        daytona.adicionaPiloto(p1);
        daytona.adicionaPiloto(p2);
        daytona.adicionaPiloto(p3);
        daytona.adicionaPiloto(p4);
        daytona.adicionaPiloto(p5);
        
        daytona.adicionaPiloto(p6);
        daytona.adicionaPiloto(p7);
        daytona.adicionaPiloto(p8);
        daytona.adicionaPiloto(p9);
        daytona.adicionaPiloto(p10);
        
        
        //System.out.println(daytona.getMapaDePilotos());
        
        daytona.iniciaCorrida();
    }
    
}

package lucaswander.cadepokemon.entidades;

import lucaswander.cadepokemon.testes.*;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;


public class DesenhaImagen {
    
    public static int MAX_TILE_SIZE = 60;
    
    
    public BufferedImage il;
    
    ArrayList<Posicao> tileList;
    
    public DesenhaImagen(BufferedImage il) {
        this.il = il;
        
        this.tileList = new ArrayList<>();
    }
    
    public ArrayList<Posicao> getTileList() {
        return tileList;
    }
    
    public void geraImagen() {
        
        int maxX = 800-(DesenhaImagen.MAX_TILE_SIZE-CadePokemon.getLevel());
        int maxY = 600-(DesenhaImagen.MAX_TILE_SIZE-CadePokemon.getLevel());
        
        int x = new Random().nextInt(maxX);
        int y = new Random().nextInt(maxY);
        
        Posicao t = new Posicao((DesenhaImagen.MAX_TILE_SIZE-CadePokemon.getLevel()*10), (DesenhaImagen.MAX_TILE_SIZE-CadePokemon.getLevel()*10), x, y);
        t.setTileImage(il);
        this.tileList.add(t);
        
    }
    
    public void desenhaImgem(int round, Graphics2D g) throws InterruptedException {
        
        Posicao t = tileList.get(round);
        
        g.drawImage(t.getTileImage(), t.posX, t.posY, t.width, t.height, null);
        Thread.sleep(1000);
        g.clearRect(t.posX, t.posY, t.width, t.height);
        
    }

}

package lucaswander.cadepokemon.entidades;

import lucaswander.cadepokemon.testes.CadePokemon;

public class ContaTempo implements Runnable{
    
    @Override
    public void run() {
        
        int count = 0;
       
        while(count != 2) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                return;
            }
            count++;
        }
        
        synchronized(CadePokemon.getAux1()) {
            CadePokemon.getAux1().notify();
        }
        
    }
}

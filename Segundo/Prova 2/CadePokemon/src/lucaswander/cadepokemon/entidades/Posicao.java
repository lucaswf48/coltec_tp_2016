package lucaswander.cadepokemon.entidades;

import java.awt.image.BufferedImage;


public class Posicao {
    
    int height;
    int width;
    
    int posX;
    int posY;
    
    private BufferedImage tile_image;
    
    protected Posicao(int height, int width, int posX, int posY) {
        this.height = height;
        this.width = width;
        this.posX = posX;
        this.posY = posY;
    }
    
    public void setTileImage(BufferedImage image) {
        this.tile_image = image;
    }
    
    public BufferedImage getTileImage() {
        return this.tile_image;
    }
    
    public boolean isInside(int x, int y) {
        return ( (x > this.posX && x < (this.posX + this.width)) && (y > this.posY && y < (this.posY + this.height)) );
    }
}

package lucaswander.cadepokemon.testes;

import java.awt.Color;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import lucaswander.cadepokemon.entidades.*;

public class CadePokemon extends Frame{

    
    
    
    private BufferedImage pokemon;
    private DesenhaImagen tb;
    
    private static int WIDTH = 800;
    private static int HEIGHT = 600;
    
    private Thread gameThread;
    
    private static Object aux1 = new Object();
    private static Object aux2 = new Object();

    private static int MAX_LEVEL = 59;
    private static boolean isGameOver = false;
    private static int level = 0;
    private static int playerScore = 0;

    private int round = 0;

    private boolean isBlinking = false;
    private Thread counterThread;
    
    
    public CadePokemon() throws IOException{
        
        super("Cade Pokemon");
        
        this.setImageLoader();
        
        tb = new DesenhaImagen(this.getPokemon());
        
        this.initComponents();

        tb.geraImagen();
        
        this.gameThread = new Thread(new Runnable() {

            @Override
            public void run() {

                while (true) {
                    try {

                        setIsBlinking(true);

                        getTb().geraImagen();
                        getTb().desenhaImgem(getRound(), (Graphics2D) getGraphics());
                        setRound(getRound() + 1);
                        if(getRound() == 5) {
                            setLevel(getLevel() + 1);
                            setRound(0);
                        }

                        setIsBlinking(false);

                        ContaTempo c = new ContaTempo();
                        setCounterThread(new Thread(c));
                        getCounterThread().start();

                        synchronized (getAux1()) {
                            getAux1().wait();
                        }

                    } catch (InterruptedException ex) {
                        Logger.getLogger(CadePokemon.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }

            }
        });

        addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {

                if (!isIsBlinking()) {

                    getCounterThread().interrupt();
                    
                    for (Posicao t : getTb().getTileList()) {
                        if (t.isInside(e.getX(), e.getY())) {
                            CadePokemon.setPlayerScore(10 + CadePokemon.getPlayerScore());
                            synchronized (getAux1()) {
                                getAux1().notify();
                            }
                            
                        } 
                        else {
                            try {
                                
                                getTb().desenhaImgem(getRound()-1, (Graphics2D)getGraphics());
                            } catch (InterruptedException ex) {
                                Logger.getLogger(CadePokemon.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        System.out.println(CadePokemon.getPlayerScore());    
                    }

                }

            }

        });

        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }

        });
        
    }
    public void setImageLoader() throws IOException {
        this.setPokemon(ImageIO.read(new File("picachu.jpeg")));
    }
    
    private void iniciaJogo() {
        
        this.getGameThread().start();
    }
    
    private void initComponents() {

        setSize(getWIDTH(), getHEIGHT());
        setResizable(false);
        setBackground(Color.WHITE);
        setVisible(true);

    }
    
    public static void main(String[] args) {
        
        try{
            
            CadePokemon c = new CadePokemon();
            JOptionPane.showMessageDialog(null, "O jogo irá começar!", "", JOptionPane.INFORMATION_MESSAGE);
            c.iniciaJogo();
            
        }catch(HeadlessException | IOException ex ){
             Logger.getLogger(CadePokemon.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * @return the pokemon
     */
    public BufferedImage getPokemon() {
        return pokemon;
    }

    /**
     * @param pokemon the pokemon to set
     */
    public void setPokemon(BufferedImage pokemon) {
        this.pokemon = pokemon;
    }

    /**
     * @return the tb
     */
    public DesenhaImagen getTb() {
        return tb;
    }

    /**
     * @param tb the tb to set
     */
    public void setTb(DesenhaImagen tb) {
        this.tb = tb;
    }

    /**
     * @return the gameThread
     */
    public Thread getGameThread() {
        return gameThread;
    }

    /**
     * @param gameThread the gameThread to set
     */
    public void setGameThread(Thread gameThread) {
        this.gameThread = gameThread;
    }

    /**
     * @return the round
     */
    public int getRound() {
        return round;
    }

    /**
     * @param round the round to set
     */
    public void setRound(int round) {
        this.round = round;
    }

    /**
     * @return the isBlinking
     */
    public boolean isIsBlinking() {
        return isBlinking;
    }

    /**
     * @param isBlinking the isBlinking to set
     */
    public void setIsBlinking(boolean isBlinking) {
        this.isBlinking = isBlinking;
    }

    /**
     * @return the counterThread
     */
    public Thread getCounterThread() {
        return counterThread;
    }

    /**
     * @param counterThread the counterThread to set
     */
    public void setCounterThread(Thread counterThread) {
        this.counterThread = counterThread;
    }
    
    /**
     * @return the WIDTH
     */
    public static int getWIDTH() {
        return WIDTH;
    }

    /**
     * @param aWIDTH the WIDTH to set
     */
    public static void setWIDTH(int aWIDTH) {
        WIDTH = aWIDTH;
    }

    /**
     * @return the HEIGHT
     */
    public static int getHEIGHT() {
        return HEIGHT;
    }

    /**
     * @param aHEIGHT the HEIGHT to set
     */
    public static void setHEIGHT(int aHEIGHT) {
        HEIGHT = aHEIGHT;
    }

    /**
     * @return the aux1
     */
    public static Object getAux1() {
        return aux1;
    }

    /**
     * @param aStaff1 the aux1 to set
     */
    public static void setAux1(Object aStaff1) {
        aux1 = aStaff1;
    }

    /**
     * @return the aux2
     */
    public static Object getAux2() {
        return aux2;
    }

    /**
     * @param aStaff2 the aux2 to set
     */
    public static void setAux2(Object aStaff2) {
        aux2 = aStaff2;
    }

    /**
     * @return the MAX_LEVEL
     */
    public static int getMAX_LEVEL() {
        return MAX_LEVEL;
    }

    /**
     * @param aMAX_LEVEL the MAX_LEVEL to set
     */
    public static void setMAX_LEVEL(int aMAX_LEVEL) {
        MAX_LEVEL = aMAX_LEVEL;
    }

    /**
     * @return the isGameOver
     */
    public static boolean isIsGameOver() {
        return isGameOver;
    }

    /**
     * @param aIsGameOver the isGameOver to set
     */
    public static void setIsGameOver(boolean aIsGameOver) {
        isGameOver = aIsGameOver;
    }

    /**
     * @return the level
     */
    public static int getLevel() {
        return level;
    }

    /**
     * @param aLevel the level to set
     */
    public static void setLevel(int aLevel) {
        level = aLevel;
    }

    /**
     * @return the playerScore
     */
    public static int getPlayerScore() {
        return playerScore;
    }

    /**
     * @param aPlayerScore the playerScore to set
     */
    public static void setPlayerScore(int aPlayerScore) {
        playerScore = aPlayerScore;
    }
  
}

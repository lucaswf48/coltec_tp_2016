
package lucaswander.banco.conta;

public class ValorInvalidoException extends RuntimeException{

    private double valorInvalido;
    
    ValorInvalidoException(double valor){
        
        this.valorInvalido = valor;
    }

    public double getValorInvalido() {
        return valorInvalido;
    }
    

}

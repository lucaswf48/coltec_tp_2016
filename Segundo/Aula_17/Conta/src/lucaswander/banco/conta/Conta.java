
package lucaswander.banco.conta;

import lucaswander.banco.conta.ValorInvalidoException;

public class Conta {
   
    protected static long numeroDeContas;
    protected String cliente;
    protected double saldo;
    protected long numeroDaConta;
    
    
    public Conta(){
        this.numeroDaConta = numeroDeContas;
        numeroDeContas++;
    }
    
    
    
    public double getSaldo(){
        return this.saldo;
    }
    
    
    public void deposita(double valor){
        this.saldo += valor;
    }
    
    public void saca(double valor){
        if(valor <= this.saldo){
            this.saldo -= valor;
        }
            
        else{
            throw new ValorInvalidoException(valor);
        }
    }
    
    public void atualiza(double taxa){
        this.saldo += this.saldo * taxa;
    }
    
    public String toString(){
        
        //System.out.println("O saldo da conta é: " + this.saldo);
        
        return Double.toString(this.saldo);
    }
    
    
    
    public boolean equals(Conta c){
        
        return this.getNumeroDaConta() == c.getNumeroDaConta() && this.getCliente().equals(c.getCliente());
    }

    /**
     * @return the numeroDaConta
     */
    public long getNumeroDaConta() {
        return numeroDaConta;
    }

    /**
     * @return the cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * @param cliente the cliente to set
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }
}

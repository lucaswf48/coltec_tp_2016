
package lucaswander.banco.conta;

import java.util.HashMap;
import java.util.Map;


public class Banco {

    //private Conta [] arrayContas = new Conta[1];
    
    private Map<String,Conta> mapaDeContas = new HashMap<>();
    
    private static int totalDeContas = 0;
    
    
    
    public Map<String,Conta> getMapaDeContas(){
        return this.mapaDeContas;
    }
    
    static public int pegaTotalDeContas(){
        
        return Banco.totalDeContas;
    }
    
    public void adicionaConta(String nome,Conta c){
        
        this.mapaDeContas.put(nome, c);
    }
    
    public Conta buscaPorNome(String nome){
        
        //System.out.println(this.mapaDeContas);
        return this.mapaDeContas.get(nome);
    }
    
//    public Conta pegaConta(int x){
//        
//        return this.arrayContas[x];
//    }
//    
//    
//    public void adiciona(Conta c){
//        
//        Banco.totalDeContas++;
//        
//        for(int i = 0; i < this.arrayContas.length;i++){
//            if(this.arrayContas[i] == null){
//                this.arrayContas[i] = c;
//                break;
//            }
//        }
//        
//        Conta [] aux = new Conta[this.arrayContas.length];
//        
//        System.arraycopy(this.arrayContas, 0, aux, 0, this.arrayContas.length);
//        
//        this.arrayContas = new Conta[this.arrayContas.length + 1];
//        
//        System.arraycopy(aux, 0, this.arrayContas, 0, aux.length);
//        
//        
//        
//    }
}

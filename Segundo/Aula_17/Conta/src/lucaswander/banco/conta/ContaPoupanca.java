
package lucaswander.banco.conta;

public class ContaPoupanca extends Conta implements Comparable<ContaPoupanca>{
    
    @Override
    public void atualiza(double taxa){
        this.saldo += this.saldo * (3*taxa);
    }

//    @Override
//    public int compareTo(ContaPoupanca t) {
//        
//        if(this.numeroDaConta > t.getNumeroDaConta())
//            return 1;
//        else if(this.numeroDaConta < t.getNumeroDaConta())
//            return -1;
//        
//        return 0;
//    }
     @Override
    public int compareTo(ContaPoupanca t) {
        
         
        if(this.cliente.compareTo(t.getCliente()) > 0)
            return 1;
        else if(this.cliente.compareTo(t.getCliente()) < 0)
            return -1;
        
        return 0;

    }
}

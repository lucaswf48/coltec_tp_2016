/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.banco.testes;

import lucaswander.banco.conta.*;

/**
 *
 * @author Lucas
 */
public class testeBuscaPorNome {
    
    public static void main(String[] args) {
        
        Banco banco = new Banco();
        Conta c1 = new ContaCorrente();
        Conta c2 = new ContaCorrente();
        
        c1.setCliente("Lucas");
        c2.setCliente("Teste");
        
        
        
        banco.adicionaConta("Lucas", c1);
         banco.adicionaConta("Teste", c2);
        
        System.out.println(banco.getMapaDeContas());

        System.out.println(banco.buscaPorNome("Lucas"));
        System.out.println(banco.buscaPorNome("Teste"));
        
        
    }
}

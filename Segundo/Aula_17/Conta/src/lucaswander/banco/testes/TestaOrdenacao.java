/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.banco.testes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import lucaswander.banco.conta.*;

/**
 *
 * @author lucas
 */
public class TestaOrdenacao {
    
    public static void main(String[] args) {
        
        List<ContaPoupanca> lista = new ArrayList < >();
        
        ContaPoupanca c1 = new ContaPoupanca();
        ContaPoupanca c2 = new ContaPoupanca();
        ContaPoupanca c3 = new ContaPoupanca();
        ContaPoupanca c4 = new ContaPoupanca();
        
        c1.setCliente("daw");
        c2.setCliente("dew");
        c3.setCliente("diw");
        c4.setCliente("dow");
        
        lista.add(c4);
        lista.add(c3);
        lista.add(c2);
        lista.add(c1);
        
        for(int i = 0; i < lista.size(); ++i){
            System.out.println(lista.get(i).getCliente());
        }
        

        System.out.println("--------------------------------------------------");
        
        Collections.sort(lista);
        //Collections.reverse(lista);
        
        for(int i = 0; i < lista.size(); ++i){
            System.out.println(lista.get(i).getCliente());
        }
    }
}

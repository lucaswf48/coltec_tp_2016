/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.banco.testes;

import java.util.HashMap;
import java.util.Map;
import lucaswander.banco.conta.ContaCorrente;

/**
 *
 * @author Lucas
 */
public class TestaMapa {
    
    public static void main(String[] args) {
        
        ContaCorrente c1 = new ContaCorrente();
        c1.deposita(10000);
        
        ContaCorrente c2 = new ContaCorrente();
        c2.deposita(3000);
        
        Map<String, ContaCorrente> mapaDeContas = new HashMap<>();
        
        mapaDeContas.put("diretor", c1);
        mapaDeContas.put("gerente",c2);
        
        ContaCorrente contaDoDiretor = mapaDeContas.get("diretor");
        
        System.out.println(contaDoDiretor.getSaldo());
    }
}

Coesão:
	
	É um princípo de engenharia de software, que se refere ao relacionamento que os membros de um módulo possuem. 
	
	Um codigo coeso e aquele que tem um relação forte, onde seus membros estão intimamente ligados e estão ali por um objetivo comum. Membros que não são absolutamente necessários para aquele módulo não devem estar presentes em códigos coesos.

	Módulos coesos são aqueles que possuem poucas responsabilidades. Desta forma a manutenção é mais simples e evita efeitos colaterais. Fica mais fácil alterar uma parte da aplicação sem afetar outras partes.

Acoplamento:
	
	Outro princípio de engenharia de software, que Se refere ao relacionamento entre os módulos. Indica quanto uma módulo depende de outro para funcionar. Códigos desacoplados são aqueles de relação fraca, e não dependem de outros para fazer sua funcionalidade básica. É sempre desejável um baixo nível de acoplamento.

	Quando há baixo acoplamento, a aplicação fica mais flexível, reusável e mais organizada. É possível intercambiar partes com problemas, ultrapassadas ou que exigem novas funcionalidades.
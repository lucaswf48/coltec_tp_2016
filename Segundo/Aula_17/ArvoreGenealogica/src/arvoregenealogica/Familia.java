/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arvoregenealogica;

import java.util.TreeMap;

/**
 *
 * @author Lucas
 */
public class Familia {
    
    private TreeMap<String, Pessoa> arvore = new TreeMap<>();
    private String nome;

    public Familia() {
    }

    public Familia(String nome) {
        this.nome = nome;
    }
    
    public void adicionaPessoa(String s,Pessoa p){
        
        this.arvore.put(s,p);
    }
    
    public Pessoa getPessoa(String s){
        
        return this.arvore.get(s);
    }
    
    public TreeMap<String, Pessoa> getArvore() {
        return arvore;
    }

    public void setArvore(TreeMap<String, Pessoa> arvore) {
        this.arvore = arvore;
    }
        
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arvoregenealogica;

import java.util.Iterator;

/**
 *
 * @author Lucas
 */
public class TestaArvoreGenealogica {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Familia familia = new Familia("Freitas");
        
        Pessoa p1 = new Pessoa("Lucas",17);
        Pessoa p2 = new Pessoa("Luca",18);
        Pessoa p3 = new Pessoa("Luc",19);
        Pessoa p4 = new Pessoa("Lu",20);
        Pessoa p5 = new Pessoa("L",21);
        
        familia.adicionaPessoa(p5.getNome(), p5);
        familia.adicionaPessoa(p4.getNome(), p4);
        familia.adicionaPessoa(p2.getNome(), p2);
        familia.adicionaPessoa(p1.getNome(), p1);
        familia.adicionaPessoa(p3.getNome(), p3);
        
        
        
        
//        System.out.println(familia.getArvore().containsKey("Lucs"));
//        System.out.println(familia.getArvore());
    }
    
}

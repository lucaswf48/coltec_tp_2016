/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arvoregenealogica;

/**
 *
 * @author Lucas
 */
public class Pessoa {
    
    private int idade;
    private String nome;
    
    Pessoa(){
        
    }
    
    Pessoa(int i){
        this.idade = i;
    }
    
    Pessoa(String s){
        this.nome = s;
    }
    
    Pessoa(int i,String s){
        this.nome = s;
        this.idade = i;
    }
    Pessoa(String s,int i){
        this.nome = s;
        this.idade = i;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}


package cadeAchou;

import lucaswander.recuperacao_2t.testes.ConfereClick;
import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.io.File;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JOptionPane;


public class CadeAchou extends Frame implements MouseListener {

    private BufferedImage mImage;
    private ArrayList<Integer> sequencia;
    private int aux;
    private int numJogada;
    private boolean win;
    private boolean terminouDeMostrar;
    private Image[] imgs;
    private long click;
    private int x;
    private int y;
    private int tamImagem;
    private int tempo;

    public static void main(String[] args) throws IOException {
        new CadeAchou();
    }

    public CadeAchou() throws IOException {
        setSize(800, 600);
        setVisible(true);
        setResizable(false);
        aux = 0;
        tamImagem = 60;
        numJogada = 0;
        tempo = 2000;
        win = false;
        terminouDeMostrar = false;
        sequencia = new ArrayList<>();

        imgs = new Image[1];
        imgs[0] = ImageIO.read(new File("res/img2.png"));

        addMouseListener(this);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });

        new Thread(new ConfereClick(this)).start();
        
        this.principal();
    }

    public void paint(Graphics g) {
    }

    public void highlightPicture() {
        Graphics2D g = (Graphics2D) this.getGraphics();
        g.clearRect(0, 0, 800, 600);
        this.tocarSom("res/sound2.wav");
        g.drawImage(imgs[0], x, y, null);
        try {
            Thread.sleep(tempo);
        } catch (InterruptedException ex) {
        }
        g.clearRect(0, 0, 800, 600);

    }

    public void principal() {
        this.terminouDeMostrar = false;
        if (this.numJogada == 20) {
            JOptionPane.showMessageDialog(this, "FIM DE JOGO\nPontuação: " + getAux() * 10);
            System.exit(0);
        }
        if (this.numJogada >= 5) {
            imgs[0] = imgs[0].getScaledInstance(30, 30, 1);
            tamImagem = 30;
            tempo = 1000;
        }
        if(this.numJogada >= 10) {
            imgs[0] = imgs[0].getScaledInstance(15, 15, 1);
            tamImagem = 15;
            tempo = 500;
        }
        if(this.numJogada >= 15) {
            imgs[0] = imgs[0].getScaledInstance(7, 7, 1);
            tamImagem = 7;
            tempo = 250;
        }
        numJogada++;
        terminouDeMostrar = false;
        Random gerador = new Random();
        x = gerador.nextInt(741);
        y = gerador.nextInt(541);

        this.highlightPicture();

        click = System.currentTimeMillis();
        terminouDeMostrar = true;

    }

    @Override
    public void mouseClicked(MouseEvent e) {
        boolean errou = true;
        if (this.isTerminouDeMostrar() == true) {
            int xClick = e.getX();
            int yClick = e.getY();
            Graphics2D g = (Graphics2D) this.getGraphics();
            g.drawImage(imgs[0], x, y, null);
            if (xClick > x && xClick < x + tamImagem) {
                if (yClick > y && yClick < y + tamImagem) {
                    errou = false;
                }
            }
            if (errou == true) {
//                this.tocarSom("res/sound.wav");
                this.terminouDeMostrar = false;
                JOptionPane.showMessageDialog(this, "<html><b>Errou!</b> <span style='color: red;'>+0</span><br>Pontuação: " + getAux() * 10+"</html>");
            } else {
                aux++;
//                this.tocarSom("res/sound.wav");
                this.terminouDeMostrar = false;
                JOptionPane.showMessageDialog(this, "<html><b>Acertou!</b> <span style='color: green;'>+10</span><br>Pontuação: " + getAux() * 10+"</html>");
            }
            this.principal();
        }
    }

    public void tocarSom(String s) {
        try {
            AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(s).getAbsoluteFile());
            Clip clip = AudioSystem.getClip();
            clip.open(audioInputStream);
            clip.start();
        } catch (Exception ex) {
        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    public long getClick() {
        return click;
    }

    public void setAux(int aux) {
        this.aux = aux;
    }

    public void setNumJogada(int numJogada) {
        this.numJogada = numJogada;
    }

    public boolean isTerminouDeMostrar() {
        return terminouDeMostrar;
    }

    public void setSequencia(ArrayList<Integer> sequencia) {
        this.sequencia = sequencia;
    }

    public int getAux() {
        return aux;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cadeAchou;



import javax.swing.JOptionPane;

/**
 *
 * @author Renato
 */
public class ConfereClick implements Runnable{
    
    Testa g;
    
    public ConfereClick(Testa g){
        this.g = g;
    }
    
    @Override
    public void run() {
        while(true){
            if(System.currentTimeMillis() - g.getClick() > 5000 && g.isTerminouDeMostrar()){
                
                JOptionPane.showMessageDialog(g, "Demorou mais de 5 segundos para clicar!");
                g.principal();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
        }
    }
    
}

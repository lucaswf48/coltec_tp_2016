
package prova1;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author lucas
 */
public class SeteReinos extends Thread{
    
    private List pessoas;
    private int numeroDePessoas = 0;
    private VaiMorrer v;

    public SeteReinos() {
    
        this.pessoas = new ArrayList();
    }
    
    public void run(){
        
        
        this.adicionaPessoa();
        
    }
    
 
    private void adicionaPessoa() {
        
        Random r1 = new Random();
        
        int k = Math.abs(r1.nextInt() % 101);
        
        
        for(int i = 0;i < k; ++i){
            
            this.setNumeroDePessoas(this.getNumeroDePessoas() + 1);
            
            this.getPessoas().add(this.getNumeroDePessoas());
            
            System.out.println("Adicionado: " + this.getNumeroDePessoas());
            
            try{
                Thread.sleep((int)(Math.random() * 100));
            }
            catch(InterruptedException e){
            
            }
        }
    }

    /**
     * @return the pessoas
     */
    public List getPessoas() {
        return pessoas;
    }

    /**
     * @param pessoas the pessoas to set
     */
    public void setPessoas(List pessoas) {
        this.pessoas = pessoas;
    }

    /**
     * @return the numeroDePessoas
     */
    public int getNumeroDePessoas() {
        return numeroDePessoas;
    }

    /**
     * @param numeroDePessoas the numeroDePessoas to set
     */
    public void setNumeroDePessoas(int numeroDePessoas) {
        this.numeroDePessoas = numeroDePessoas;
    }

    /**
     * @return the v
     */
    public VaiMorrer getV() {
        return v;
    }

    /**
     * @param v the v to set
     */
    public void setV(VaiMorrer v) {
        this.v = v;
    }
    
}

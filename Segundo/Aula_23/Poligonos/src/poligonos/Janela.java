/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package poligonos;

import java.awt.Frame;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.GeneralPath;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class Janela extends Frame{
    
    
    
    public Janela(){
        super("Exemplo Java2D");
        setSize(1000,1000);
        setVisible(true);
        addWindowListener(new WindowAdapter() 
        { public void windowClosing(WindowEvent e){
            dispose();System.exit(0);
        }});
    }
    
    
    public Janela(double angulo){
        super("Exemplo Java2D");
        setSize(1000,1000);
        setVisible(true);
        addWindowListener(new WindowAdapter() 
        { public void windowClosing(WindowEvent e){
            dispose();System.exit(0);
        }});
    }
    
    public void paint(Graphics g){
        
//        int n = 6;
        int n = Integer.parseInt(JOptionPane.showInputDialog(null, "Digite o numero: "));;
        
        double angulo = 360/n;
        
        double anguloAux = (n % 2 == 0) ? 90 - (angulo / 2) : 90; 

        int []x1Points = new int[n];
        int []y1Points = new int[n];
        
        
//        int x1Points[] = {50,150,200,150,50,0};
//        int y1Points[] = {273,273,187,100,100,187};        
//        for(int i = 0; i < n; i++){
//            double rad = Math.toRadians(anguloAux);
//            x1Points[i] = (int) (Math.sin(rad)*100)+300;
//            System.out.println(x1Points[i]);
//            y1Points[i] = (-(int) (Math.cos(rad)*100))+300;
//            anguloAux += angulo;
//        }

          for(int i = 0; i < n; i++){
            double rad = Math.toRadians(anguloAux);
            
            
            x1Points[i] = (int) (Math.cos(rad)*300)+400;
            System.out.println("x1: " + x1Points[i]);
            y1Points[i] = (-(int) (Math.sin(rad)*300))+400;
            System.out.println("y1: " + y1Points[i]);
            anguloAux += angulo;
            
        }
        
        
        Graphics2D g2 = (Graphics2D)g;
        
        
        
        GeneralPath polygon = new GeneralPath(GeneralPath.WIND_EVEN_ODD,x1Points.length);
        polygon.moveTo(x1Points[0], y1Points[0]);
        
        for(int index = 1; index < x1Points.length; index++){
            polygon.lineTo(x1Points[index],y1Points[index]);
        }
        
        polygon.closePath();
        g2.fill(polygon);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.aula23exemplos.principal;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.geom.Arc2D;
import static java.awt.geom.Arc2D.PIE;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import java.awt.Image;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.awt.image.RescaleOp;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;


/**
 *
 * @author Lucas
 */
public class Ex1 extends Frame{
    public static void main(String[] args) {
        new Ex1();
    }

    public Ex1(){
        
        super("Exemplo Java2D");
        setSize(1000,1000);
        setVisible(true);
        addWindowListener(new WindowAdapter() 
        { public void windowClosing(WindowEvent e){
            dispose();System.exit(0);
        }});
    }
    
    public void paint(Graphics g){
        
        
//        g2d.setColor(Color.blue);
//        //g2d.drawRect(50,50,300,300);
//        g2d.setColor(Color.red);
//        g2d.fill(new Ellipse2D.Double(80,30,65,100));
//        g2d.setColor(Color.yellow);
       
        
//        //g2d.draw(new RoundRectangle2D.Double(155,30,75,100,50,50));
////        g2d.setColor(Color.ORANGE);
////        g2d.draw(new Arc2D.Double(5,150,70,70,0,270,Arc2D.PIE));
////        g2d.setColor(Color.pink);
////        g2d.draw(new Line2D.Double(80,150,200,200));

//        g2d.setPaint(Color.red);
//        g2d.fill(new Ellipse2D.Double(5,30,65,100));
//        g2d.setPaint(new GradientPaint(5,30,Color.red,35,100,Color.blue,true));
//        g2d.fill(new Ellipse2D.Double(100,30,65,100));
        
//        BufferedImage img = null;
//        try{
//            img = ImageIO.read(new File("daw.jpg"));
//        }catch(IOException e){
//            e.printStackTrace();
//        }
//       g2d.setColor(Color.CYAN);
//       g2d.draw(new Rectangle2D.Double(100,100,500,500));
//       g2d.drawImage(img,100,125,500,500,null);
        
        //g2d.drawImage(img,rap,500,0);
        
//        Graphics2D g2d = (Graphics2D)g;
//        g2d.setPaint(Color.red);
//        g2d.setStroke(new BasicStroke(4.0f));
//        g2d.draw(new Rectangle2D.Double(5,30,65,100));
//        
//        g2d.setPaint(Color.blue);
//        
//        float dashes[] = {8};
//        
//        g2d.setStroke(new BasicStroke(4,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND,10,dashes,0));
//        g2d.draw(new Line2D.Double(160,30,75,100));
//        
//        g2d.setPaint(Color.darkGray);
//        
//        g2d.setStroke(new BasicStroke(7,BasicStroke.CAP_ROUND,BasicStroke.JOIN_BEVEL));
//        g2d.draw(new Rectangle2D.Double(200,30,65,100));

        Graphics2D g2 = (Graphics2D)g;
//        
//        double x = 15, y = 50, w = 70, h = 70;
//        
//        Ellipse2D e = new Ellipse2D.Double(x,y,w,h);
//        g2.setStroke(new BasicStroke(8));
//        Color smokey = new Color(128,128,128,128);
//        g2.setPaint(smokey);
//        g2.fill(e);
//        g2.draw(e);
//        e.setFrame(x + 100, y,w,h);
//        g2.setPaint(Color.black);
//        g2.draw(e);
//        g2.setPaint(Color.gray);
//        g2.fill(e);
//        e.setFrame(x + 200, y,w,h);
//        g2.setPaint(Color.gray);
//        g2.draw(e);
//        g2.setPaint(Color.black);
//        g2.fill(e);
        


        //int x1Points[] = {25,75,100,50,0};
        //int y1Points[] = {200,200,125,100,125};
        
        int x1Points[] = {50,150,200,150,50,0};
        int y1Points[] = {273,273,187,100,100,187};
        
        GeneralPath polygon = new GeneralPath(GeneralPath.WIND_EVEN_ODD,x1Points.length);
        polygon.moveTo(x1Points[0], y1Points[0]);
        
        for(int index = 1; index < x1Points.length; index++){
            polygon.lineTo(x1Points[index],y1Points[index]);
        }
        
        polygon.closePath();
        g2.fill(polygon);
    }
   
}

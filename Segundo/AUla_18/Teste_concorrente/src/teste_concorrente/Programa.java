/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package teste_concorrente;

/**
 *
 * @author lucas
 */
public class Programa implements Runnable{
    
    private int id;
    
    public void run() {
        
        for(int i = 0; i < 10000; i++){
            System.out.println("Programa " + this.id + " valor: " + i);
        }
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }
    
}

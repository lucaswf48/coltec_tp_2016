/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corridadesapos;

/**
 *
 * @author strudel
 */
public class CorridaDeSapos {

    /**
     * @param args the command line arguments
     */
    final static int NUM_SAPOS = 5; // QTE. de sapos na corrida

    final static int DISTANCIA = 500; // Distância da corrida em cm
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        for(int i = 1; i <= NUM_SAPOS; ++i){
            new SapoCorrendoThread("SAPO_" + i, DISTANCIA).start();
        }
    }
    
}

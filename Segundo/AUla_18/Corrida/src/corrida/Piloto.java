/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corrida;

import java.util.Random;

/**
 *
 * @author Lucas
 */
public class Piloto extends Thread{
    
    private String nome;
    private double distanciaPulo;
    private double distanciaPercorrida;
    private double distanciaTotalDaCorrida;
    static int colocacao = 0;
            
    Piloto(String n,double d) {
        
        this.nome = n;
        this.distanciaTotalDaCorrida = d;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getDistanciaPulo() {
        return distanciaPulo;
    }

    public void setDistanciaPulo(double distanciaPulo) {
        this.distanciaPulo = distanciaPulo;
    }

    public double getDistanciaPercorrida() {
        return distanciaPercorrida;
    }

    public void setDistanciaPercorrida(double distanciaPercorrida) {
        this.distanciaPercorrida = distanciaPercorrida;
    }

    public double getDistanciaTotalDaCorrida() {
        return distanciaTotalDaCorrida;
    }

    public void setDistanciaTotalDaCorrida(double distanciaTotalDaCorrida) {
        this.distanciaTotalDaCorrida = distanciaTotalDaCorrida;
    }
    
    public void mostraSituacao(){
        
        System.out.println("Nome: " + this.nome + "\n" + "Distancia percorrida: " + this.distanciaPercorrida);
    }
    
    public void avanca(){
        
        Random g = new Random();
        
        if(this.distanciaPercorrida < this.distanciaTotalDaCorrida){
            
            int x = g.nextInt() % 10;
            this.distanciaPercorrida += Math.abs(x);
            
        }
    }
    
    public void sapoDescansando () {

        yield();

    }
    
    @Override
    public void run(){
        
        while(this.distanciaPercorrida < this.distanciaTotalDaCorrida){
            this.mostraSituacao();
            this.avanca();
            this.sapoDescansando();
        }
        
        this.coloacao();
    }

    private void coloacao() {
        colocacao++;
        System.out.println("Colocação final do sapo " + this.nome + " é " + colocacao);
    }
}

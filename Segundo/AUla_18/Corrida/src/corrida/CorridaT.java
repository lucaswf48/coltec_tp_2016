/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package corrida;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Lucas
 */
public class CorridaT {
    
    private double distanciaCorrida;
    private Map<String,Piloto> mapaDePilotos= new HashMap<>();

    public CorridaT(double distanciaCorrida) {
        this.distanciaCorrida = distanciaCorrida;
    }

    
    
    public double getDistanciaCorrida() {
        return distanciaCorrida;
    }

    public void setDistanciaCorrida(double distanciaCorrida) {
        this.distanciaCorrida = distanciaCorrida;
    }

    public Map<String,Piloto> getMapaDePilotos() {
        return mapaDePilotos;
    }

    public void setMapaDePilotos(Map<String,Piloto> mapaDePilotos) {
        this.mapaDePilotos = mapaDePilotos;
    }

    public void adicionaPiloto(Piloto p1) {
        
        this.mapaDePilotos.put(p1.getNome(), p1);
    }
    
    public void iniciaCorrida(){
        
        for(String s : this.mapaDePilotos.keySet())
            mapaDePilotos.get(s).start();
    }
}

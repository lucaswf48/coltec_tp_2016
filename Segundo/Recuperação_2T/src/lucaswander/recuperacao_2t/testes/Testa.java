/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lucaswander.recuperacao_2t.testes;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import lucaswander.playaudio.PlayAudio;
/**
 *
 * @author lucas
 */
public class Testa extends Frame implements MouseListener{

    
    private int tamImagem;
    private Image[] imgs;
    private int[][] posicao;
    private boolean terminouDeMostrar;
    private int numJogada;
    private boolean win;
    private int aux;
    private int imagenSize;
    private int numeroImagens;
    private long click;
    private int xdiferente;
    private int ydiferente;
    
    
    public Testa() throws IOException {
        
        setSize(800, 600);
        setVisible(true);
        setResizable(false);
        
        this.tamImagem = 60;
        this.numJogada = 1;
        this.win = false;
        this.aux = 0;
        this.imagenSize = 40;
                
        this.imgs = new Image[2];
        imgs[0] = ImageIO.read(new File("src/lucaswander/recuperacao_2t/testes/jaoOriginal.jpg"));
        imgs[1] = ImageIO.read(new File("src/lucaswander/recuperacao_2t/testes/daw.jpg"));
        imgs[0] = imgs[0].getScaledInstance(this.imagenSize, this.imagenSize, 1);
        imgs[1] = imgs[1].getScaledInstance(this.imagenSize, this.imagenSize, 1);
        
        addMouseListener(this);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });
        
        new Thread(new ConfereClick(this)).start();
        
        this.principal();
    }

    public static void main(String[] args) throws IOException {
        new Testa();
    }
    
    public boolean isTerminouDeMostrar() {
        return terminouDeMostrar;
    }
    
    @Override
    public void mouseClicked(MouseEvent e) {
        
        boolean errou = true;
        if (this.isTerminouDeMostrar() == true) {
            int xClick = e.getX();
            int yClick = e.getY();
        
            System.out.println("dawdasdas");
            //g.drawImage(imgs[0], x, y, null);
            
            if (xClick > xdiferente && xClick < xdiferente + tamImagem) {
                if (yClick > ydiferente && yClick < ydiferente + tamImagem) {
                    errou = false;
                }
            }
            if (errou == true) {
//                this.tocarSom("res/sound.wav");
                this.terminouDeMostrar = false;
                JOptionPane.showMessageDialog(this, "<html><b>Errou!</b> <span style='color: red;'>+0</span><br>Pontuação: " + getAux() * 10+"</html>");
            } else {
                aux++;
//                this.tocarSom("res/sound.wav");
                this.terminouDeMostrar = false;
                JOptionPane.showMessageDialog(this, "<html><b>Acertou!</b> <span style='color: green;'>+10</span><br>Pontuação: " + getAux() * 10+"</html>");
            }
            this.principal();
        }
    }


    public long getClick() {
        return click;
    }
    
    public void principal() {
        
        if (this.numJogada == 6) {
            JOptionPane.showMessageDialog(this, "FIM DE JOGO\nPontuação: " + getAux() * 10);
            System.exit(0);
        }
        
        if (this.numJogada == 1) {
            
            this.numeroImagens = 2;
            
            this.posicao = new int[2][2];
            
            this.posicao[0][0] = 0;
            this.posicao[0][1] = 40;
            this.posicao[1][0] = 40;
            this.posicao[1][1] = 40;
            
                    
            
            //tempo = 1000;
        }
//        
        if (this.numJogada == 2) {
            
            this.numeroImagens = 4;
            
            this.posicao = new int[4][2];
            
            this.posicao[0][0] = 0;
            this.posicao[0][1] = 40;
            
            this.posicao[1][0] = 40;
            this.posicao[1][1] = 40;
            
            this.posicao[2][0] = 0;
            this.posicao[2][1] = 80;
            
            this.posicao[3][0] = 40;
            this.posicao[3][1] = 80;
        }
        if(this.numJogada == 3) {
            
            this.numeroImagens = 9;
            
            this.posicao = new int[9][2];
            
            this.posicao[0][0] = 0;
            this.posicao[0][1] = 40;
            
            this.posicao[1][0] = 40;
            this.posicao[1][1] = 40;
            
            this.posicao[2][0] = 80;
            this.posicao[2][1] = 40;
            
            this.posicao[3][0] = 0;
            this.posicao[3][1] = 80;
            
            this.posicao[4][0] = 40;
            this.posicao[4][1] = 80;
            
            this.posicao[5][0] = 80;
            this.posicao[5][1] = 80;
            
            this.posicao[6][0] = 0;
            this.posicao[6][1] = 120;
            
            this.posicao[7][0] = 40;
            this.posicao[7][1] = 120;
            
            this.posicao[8][0] = 80;
            this.posicao[8][1] = 120;
            
        }
        if(this.numJogada == 4) {
            
            this.numeroImagens = 16;
            
            this.posicao = new int[16][2];
            
            this.posicao[0][0] = 0;
            this.posicao[0][1] = 40;
            
            this.posicao[1][0] = 40;
            this.posicao[1][1] = 40;
            
            this.posicao[2][0] = 80;
            this.posicao[2][1] = 40;
            
            this.posicao[3][0] = 120;
            this.posicao[3][1] = 40;
            
            this.posicao[4][0] = 0;
            this.posicao[4][1] = 80;
            
            this.posicao[5][0] = 40;
            this.posicao[5][1] = 80;
            
            this.posicao[6][0] = 80;
            this.posicao[6][1] = 80;
            
            this.posicao[7][0] = 120;
            this.posicao[7][1] = 80;
            
            this.posicao[8][0] = 0;
            this.posicao[8][1] = 120;
            
            this.posicao[9][0] = 40;
            this.posicao[9][1] = 120;
            
            this.posicao[10][0] = 80;
            this.posicao[10][1] = 120;
            
            this.posicao[11][0] = 120;
            this.posicao[11][1] = 120;
            
            this.posicao[12][0] = 0;
            this.posicao[12][1] = 160;
            
            this.posicao[13][0] = 40;
            this.posicao[13][1] = 160;
            
            this.posicao[14][0] = 80;
            this.posicao[14][1] = 160;
            
            this.posicao[15][0] = 120;
            this.posicao[15][1] = 160;
            
            
            //tempo = 250;
        }
        if (this.numJogada == 5) {
            
            this.numeroImagens = 25;
            
            this.posicao = new int[25][2];
            
            this.posicao[0][0] = 0;
            this.posicao[0][1] = 40;
            
            this.posicao[1][0] = 40;
            this.posicao[1][1] = 40;
            
            this.posicao[2][0] = 80;
            this.posicao[2][1] = 40;
            
            this.posicao[3][0] = 120;
            this.posicao[3][1] = 40;
            
            this.posicao[4][0] = 160;
            this.posicao[4][1] = 40;
            
            this.posicao[5][0] = 0;
            this.posicao[5][1] = 80;
            
            this.posicao[6][0] = 40;
            this.posicao[6][1] = 80;
            
            this.posicao[7][0] = 80;
            this.posicao[7][1] = 80;
            
            this.posicao[8][0] = 120;
            this.posicao[8][1] = 80;
            
            this.posicao[9][0] = 160;
            this.posicao[9][1] = 80;
            
            this.posicao[10][0] = 0;
            this.posicao[10][1] = 120;
            
            this.posicao[11][0] = 40;
            this.posicao[11][1] = 120;
            
            this.posicao[12][0] = 80;
            this.posicao[12][1] = 120;
            
            this.posicao[13][0] = 120;
            this.posicao[13][1] = 120;
            
            this.posicao[14][0] = 160;
            this.posicao[14][1] = 120;
            
            this.posicao[15][0] = 0;
            this.posicao[15][1] = 160;
            
            this.posicao[16][0] = 40;
            this.posicao[16][1] = 160;
            
            this.posicao[17][0] = 80;
            this.posicao[17][1] = 160;
            
            this.posicao[18][0] = 120;
            this.posicao[18][1] = 160;
            
            this.posicao[19][0] = 160;
            this.posicao[19][1] = 160;
            
            this.posicao[20][0] = 0;
            this.posicao[20][1] = 200;
            
            this.posicao[21][0] = 40;
            this.posicao[21][1] = 200;
            
            this.posicao[22][0] = 80;
            this.posicao[22][1] = 200;
            
            this.posicao[23][0] = 120;
            this.posicao[23][1] = 200;
            
            this.posicao[24][0] = 160;
            this.posicao[24][1] = 200;
            
            
            //tempo = 1000;
        }
        
        
        this.numJogada++;
        this.terminouDeMostrar = false;
        this.highlightPicture();
        this.terminouDeMostrar = true;
    }

    public int getX(int i){
       
        return this.posicao[i][0];
    }
    
    public int getY(int i){
       
        return this.posicao[i][1];
    }
    
    private void highlightPicture() {
        
        Graphics2D g = (Graphics2D) this.getGraphics();
        g.clearRect(0, 0, 800, 600);
        
        Random gerador = new Random();
        int x = gerador.nextInt(this.numeroImagens);        
        System.out.println("x" + x);
                
        for(int i = 0; i < this.numeroImagens;i++){ 
            
            if(i == x){
                g.drawImage(imgs[1], this.getX(i),this.getY(i), null);
                this.xdiferente = this.getX(i);
                this.ydiferente = this.getY(i);
            }
            else{
                g.drawImage(imgs[0], this.getX(i),this.getY(i), null);
            }
            
        }

        try {
           Thread.yield();
            Thread.sleep(2000);
        } catch (InterruptedException ex) {
        
        }
        g.clearRect(0, 0, 800, 600);
    }
    
    public int getAux() {
        return aux;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
      //  throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
       // throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
     //   throw new UnsupportedOperationException("Not supported yet.");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios3aula18;

/**
 *
 * @author Lucas
 */
public class Cont2 {
    
    private int ct;
    
    public void Cont2(){
        ct = 0;
    }
    
    public synchronized void inc(){
        while(ct >= 100){
            try{
                wait();
            }
            catch(InterruptedException e){}
        }
        ct++;
        System.out.println("Valor :" + ct);
        if(ct == 1)
            notifyAll();
    }
    
    public synchronized void dec(){
        
        while(ct <= 0){
            try{
                wait();
            }
            catch(InterruptedException e){}
        }
        ct--;
        System.out.println("Valor :" + ct);
        if(ct == 99)
            notifyAll();
    }
}

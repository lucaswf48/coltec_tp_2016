/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios3aula18;

/**
 *
 * @author Lucas
 */
public class Cont3 {
    
    private int ct;
    private String estado;
    
    public void Cont3(){
        ct = 0;
        estado = "min";
    }
    
    public synchronized void inc(){
        
        while("max".equals(this.estado)){
            try{
                wait();
            }
            catch(InterruptedException e){}
            
        }
        
        if("min".equals(this.estado)){
            notifyAll();
            estado = "med";
        }
        
        if(this.ct == 100)
            this.estado = "max";
    }
    
    public synchronized void dec(){
        while("min".equals(this.estado)){
            try{
                wait();
            }
            catch(InterruptedException e){
                
            }
        } 
        
        if("max".equals(this.estado)){
            notifyAll();
            estado = "med";
        }
        
        if(this.ct == 0)
            this.estado = "min";
    }
}

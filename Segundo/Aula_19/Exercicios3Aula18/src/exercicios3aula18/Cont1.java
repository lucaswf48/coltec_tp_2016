/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios3aula18;

/**
 *
 * @author Lucas
 */
public class Cont1 {
    
    private int ct;
    
    public void Cont1(){
        ct = 0;
    }
    
    public void inc() throws CannotIncException{
        
        if(ct == 100)
            throw new CannotIncException();
        else{
            
            ct++;
            System.out.println("Valor :" + ct);
        }
    }
    
    public void dec() throws CannotDecException{
        
        if(ct == 0)
            throw new CannotDecException();
        else{
            ct--;
            System.out.println("Valor :" + ct);
        }
    }
}

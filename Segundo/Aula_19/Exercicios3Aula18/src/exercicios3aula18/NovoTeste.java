/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exercicios3aula18;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author strudel
 */
public class NovoTeste extends Thread{
    
    Cont1 c1;
    Cont2 c2;
    Cont3 c3;

    public NovoTeste(Cont1 t1,Cont2 t2,Cont3 t3) {
        
        this.c1 = t1;
        this.c2 = t2;
        this.c3 = t3;
    }
    
    
    public void run(){
        
        for(int i = 0; i < 25; i++){
        
            try {
                this.c1.dec();
                //this.c2.dec();
                //this.c3.dec();
            } catch (CannotDecException ex) {
                //Logger.getLogger(NovoTeste.class.getName()).log(Level.SEVERE, null, ex);
                System.out.println(ex);
            }
        }
    }
}

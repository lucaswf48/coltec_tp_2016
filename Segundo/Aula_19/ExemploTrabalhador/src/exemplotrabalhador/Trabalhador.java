/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplotrabalhador;

/**
 *
 * @author Lucas
 */
public class Trabalhador extends Thread{
    
    String produto;
    int tempo;
    
    public Trabalhador(String produto, int tempo){
        this.produto = produto;
        this.tempo = tempo;
    }
    
    @Override
    public void run(){
        
        for(int i = 0; i < 50; ++i){
            System.out.println(i + " " + produto);
            try{
                sleep((long)(Math.random() * tempo));
            }catch(InterruptedException e){
                
            }
        }
        
        System.out.println("Terminei " + this.produto);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizacao;

/**
 *
 * @author Lucas
 */
public class Producer extends Thread{
    
    PilhaSincronizada pilha;

    public Producer(PilhaSincronizada pilha){
        this.pilha = pilha;
    }
    
    @Override
    public void run() {
        
        
        for(int i = 0; i < 5; i++){
            
            this.pilha.push(i);
            System.out.println("Criado: " + i);
            try{
                Thread.sleep((int)(Math.random() * 1000));
            }
            catch(InterruptedException e){
            }
        }
    }
    
}

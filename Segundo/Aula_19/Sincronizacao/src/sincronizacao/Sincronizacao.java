/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizacao;

/**
 *
 * @author Lucas
 */
public class Sincronizacao {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
     
        PilhaSincronizada p = new PilhaSincronizada();
        
        Consumer c = new Consumer(p);
        Producer pr = new Producer(p);
        
        pr.start();
        c.start();
        
    }
    
}

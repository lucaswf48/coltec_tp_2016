/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizacao;

/**
 *
 * @author Lucas
 */
public class Consumer extends Thread{
    
    PilhaSincronizada pilha;

    public Consumer(PilhaSincronizada pilha){
        this.pilha = pilha;
    }
    
    @Override
    public void run() {
        
        int valor;
        for(int i = 0; i < 5; i++){
            
            valor = this.pilha.pop();
            System.out.println("Valor " + valor);
            try{
                Thread.sleep((int)(Math.random() * 5000));
            }
            catch(InterruptedException e){
            }
        }
    }
}

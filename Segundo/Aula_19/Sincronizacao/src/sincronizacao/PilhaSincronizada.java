/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sincronizacao;

/**
 *
 * @author Lucas
 */
public class PilhaSincronizada {
    
    private int index = 0;
    private int[] buffer = new int[10];
    
//    public synchronized int pop(){
//        this.index--;
//        return this.buffer[index];
//    }
    
    public synchronized int pop(){
        
        while(this.index == 0){
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
        
        this.notify();
        
        this.index--;
        return this.buffer[index];
    }
    
//    public synchronized void push(int i){
//        this.buffer[index] = i;
//        index++;
//    }
    
    public synchronized void push(int i){
        
        while(this.index == this.buffer.length){
            try {
                this.wait();
            } catch (InterruptedException e) {
            }
        }
        
        this.notify();
        this.buffer[index] = i;
        index++;
    }
}

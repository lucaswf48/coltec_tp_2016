/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplodeadlock;

/**
 *
 * @author Lucas
 */
public class Cliente extends Thread{
    
    private String nome;
    private int op;
    private double valor;
    private Caixa cai;

    public Cliente(String nome,int o,double v,Caixa c) {
        this.nome = nome;
        this.op = o;
        this.valor = v;
        this.cai = c;
    }

    public Caixa getCai() {
        return cai;
    }
    
    public double getValor() {
        return valor;
    }
    
    
    public void setOp(int op) {
        this.op = op;
    }

    public int getOp() {
        return op;
    }
    
    public String getNome() {
        return nome;
    }
    
    @Override
    public void run(){
        
        this.getCai().atender(this, this.op, this.valor);
    }
    
    
}

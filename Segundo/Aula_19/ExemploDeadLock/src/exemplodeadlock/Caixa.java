/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplodeadlock;

/**
 *
 * @author Lucas
 */
public class Caixa {
    
    double saldoCaixa = 0.0;
    Cliente clienteDaVez = null;
    
    public synchronized void atender(Cliente c, int op, double v){
        while(clienteDaVez != null){
            try{
                
                this.wait();
                
            }catch(InterruptedException e){
                
            }
            
        }
        
        clienteDaVez = c;
        switch(op){
            case -1: this.sacar(c,v);
                     break;
            case 1: this.depositar(c,v);
                    break;
        }
    }
    
    private synchronized void sacar(Cliente c, double valor){
        
//        while(saldoCaixa <= valor){
//            
//            try {
//                
//                this.wait();
//                
//            } catch (Exception e) {
//                
//            }
//        }

        if(saldoCaixa <= valor){
            System.out.println("Saldo insuficiente\n" + "Usuario: " + c.getNome());
            clienteDaVez = null;
            return;
        }
        
        if(valor > 0){
            System.out.println("Saque realizado com sucesso.\n"  + "Usuario: " + c.getNome());
            saldoCaixa -= valor;
            clienteDaVez = null;
            notifyAll();
        }
    }
    
    private synchronized void depositar(Cliente c, double valor){
        if(valor > 0){
            
            System.out.println("Deposito realizado com sucesso.\n" + "Usuario: " + c.getNome());
            saldoCaixa += valor;
            clienteDaVez = null;
            notifyAll();
        }
    }
}

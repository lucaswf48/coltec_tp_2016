/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exemplodeadlock;

/**
 *
 * @author Lucas
 */
public class ExemploDeadLock {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Caixa c1 = new Caixa();
        
        Cliente lucas = new Cliente("Lucas",1,100.00,c1);
        Cliente ilmer = new Cliente("ilmer",-1,50.00,c1);
        
        //Normal
//        lucas.start();
//        ilmer.start();
        
        //Deadlock
        ilmer.start();
        lucas.start();
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication26;

import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author lucas
 */
public class JavaApplication26 extends Frame implements MouseListener{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws HeadlessException, IOException {
        
        new JavaApplication26();
    }
    private int numJogada;
    private int aux;
    private final Image[] imgs;
    private int imagemTamanhoX;
    private int imagemTamanhoY;
    private int numeroImagens;
    private boolean terminouDeMostrar;
    private int [][] posicao;
    private int [][] rodada;
    private long click;
    private int numClicks;
    private int numeroAcertos = 0;
    public JavaApplication26() throws HeadlessException, IOException {
        
        setSize(800, 1000);
        setVisible(true);
        setResizable(false);
        
        this.numJogada = 1;
        this.aux = 0;
        this.imagemTamanhoX = 133;
        this.imagemTamanhoY = 100;
        
        this.imgs = new Image[1];
        this.posicao = new int[36][2];
        this.definePosicoes();
        
        imgs[0] = ImageIO.read(new File("src/javaapplication26/jaoOriginal.jpg"));
        imgs[0] = imgs[0].getScaledInstance(this.imagemTamanhoX, this.imagemTamanhoY, 1);
        
        addMouseListener(this);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                dispose();
                System.exit(0);
            }
        });
        
        new Thread(new ConfereClick(this)).start();
        
        this.principal();
    }

    
    
    @Override
    public void mouseClicked(MouseEvent e) {
        
        
        boolean errou = true;
        
        
        if (this.isTerminouDeMostrar() == true) {
        
            this.numClicks++;
            int xClick = e.getX();
            int yClick = e.getY();
        
            System.out.println("x click: " + xClick);
            System.out.println("y click: " + yClick);
            
           
            for(int i = 0; i < this.numeroImagens;i++){
                

                
                if (xClick > this.rodada[i][0] && xClick < this.rodada[i][0] + this.imagemTamanhoX) {
                    if (yClick > this.rodada[i][1] && yClick < this.rodada[i][1] + this.imagemTamanhoY) {
                        errou = false;
                        numeroAcertos++;
                        break;
                    }
                }
            }
            
           
            if (errou == true) {

                this.terminouDeMostrar = false;
                JOptionPane.showMessageDialog(this, "Errou!+0Pontuação: " + getAux() * 10+"");
                
                this.principal();
            }

            if (errou == false && numeroAcertos == this.numeroImagens){
                aux++;
                this.terminouDeMostrar = false;                
                JOptionPane.showMessageDialog(this, "Acertou!+10Pontuação: " + getAux() * 10+"");
                this.principal();
            }
            

        }
    }

    @Override
    public void mousePressed(MouseEvent e) {
   //     throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseReleased(MouseEvent e) {
  //      throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseEntered(MouseEvent e) {
//        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public void mouseExited(MouseEvent e) {
   //     throw new UnsupportedOperationException("Not supported yet.");
    }

    
    public void definePosicoes(){
            this.posicao[0][0] = 0;//1
            this.posicao[0][1] = 33;//1
            
            this.posicao[1][0] = 133;//1
            this.posicao[1][1] = 33;//1
            
            this.posicao[2][0] = 266;//1
            this.posicao[2][1] = 33;//1
            
            this.posicao[3][0] = 399;//1
            this.posicao[3][1] = 33;//1
            
            this.posicao[4][0] = 532;//1
            this.posicao[4][1] = 33;//1
            
            this.posicao[5][0] = 655;//1
            this.posicao[5][1] = 33;//1
            
            this.posicao[6][0] = 0;//2
            this.posicao[6][1] = 133;//2
            
            this.posicao[7][0] = 133;//2
            this.posicao[7][1] = 133;//2
            
            this.posicao[8][0] = 266;//2
            this.posicao[8][1] = 133;//2
            
            this.posicao[9][0] = 399;//2
            this.posicao[9][1] = 133;//2
            
            this.posicao[10][0] = 532;//2
            this.posicao[10][1] = 133;//2
            
            this.posicao[11][0] = 655;//2
            this.posicao[11][1] = 133;//2
            
            
            
            this.posicao[12][0] = 0;//3
            this.posicao[12][1] = 233;//3
            
            this.posicao[13][0] = 133;//3
            this.posicao[13][1] = 233;//3
            
            this.posicao[14][0] = 266;//3
            this.posicao[14][1] = 233;//3
            
            this.posicao[15][0] = 399;//3
            this.posicao[15][1] = 233;//3
            
            this.posicao[16][0] = 532;//3
            this.posicao[16][1] = 233;//3
            
            this.posicao[17][0] = 655;//3
            this.posicao[17][1] = 233;//3
            
            this.posicao[18][0] = 0;//4
            this.posicao[18][1] = 333;//4
            
            this.posicao[19][0] = 133;//4
            this.posicao[19][1] = 333;//4
            
            this.posicao[20][0] = 266;//4
            this.posicao[20][1] = 333;//4
            
            this.posicao[21][0] = 399;//4
            this.posicao[21][1] = 333;//4
            
            this.posicao[22][0] = 532;//4
            this.posicao[22][1] = 333;//4
            
            this.posicao[23][0] = 665;//4
            this.posicao[23][1] = 333;//4
            
            
            
            this.posicao[24][0] = 0;//1
            this.posicao[24][1] = 433;//1
            
            this.posicao[25][0] = 133;//1
            this.posicao[25][1] = 433;//1
            
            this.posicao[26][0] = 266;//1
            this.posicao[26][1] = 433;//1
            
            this.posicao[27][0] = 399;//1
            this.posicao[27][1] = 433;//1
            
            this.posicao[28][0] = 532;//1
            this.posicao[28][1] = 433;//1
            
            this.posicao[29][0] = 665;//1
            this.posicao[29][1] = 433;//1
            
            
            this.posicao[30][0] = 0;//1
            this.posicao[30][1] = 533;//1
            
            this.posicao[31][0] = 133;//1
            this.posicao[31][1] = 533;//1
            
            this.posicao[32][0] = 266;//1
            this.posicao[32][1] = 533;//1
            
            this.posicao[33][0] = 399;//1
            this.posicao[33][1] = 533;//1
            
            this.posicao[34][0] = 532;//1
            this.posicao[34][1] = 533;//1
            
            this.posicao[35][0] = 665;//1
            this.posicao[35][1] = 533;//1
    }
    
    public void principal() {
        this.numeroAcertos = 0;
        
        if(this.numJogada == 1){
            this.numeroImagens = 2;
            this.rodada = new int[this.numeroImagens][2];
            
        }
        
        
        if(this.numJogada == 2){
            this.numClicks = 0;
            this.numeroImagens = 4;
            this.rodada = new int[this.numeroImagens][2];
        }
        
        if(this.numJogada == 3){
            this.numeroImagens = 6;
            this.rodada = new int[this.numeroImagens][2];
        }
        
        if(this.numJogada == 4){
            this.numeroImagens = 8;
            this.rodada = new int[this.numeroImagens][2];
        }
        
        if(this.numJogada == 5){
            JOptionPane.showMessageDialog(this, "FIM DE JOGO\nPontuação: " + getAux() * 10);
            System.exit(0);
        }
        
        
        this.numJogada++;
        this.terminouDeMostrar = false;
        this.highlightPicture();    
        click = System.currentTimeMillis();
        
        this.terminouDeMostrar = true;        
    }

    boolean isTerminouDeMostrar() {
        return terminouDeMostrar;
    }
    
    public long getClick() {
        return click;
    }
    private void highlightPicture() {
        
        Graphics2D g = (Graphics2D) this.getGraphics();
        g.clearRect(0, 0, 800, 1000);
        
        Random gerador = new Random();
        
        ArrayList<Integer> arrlist = new ArrayList<Integer>(8);
        
        
        
        int x = 0;
        
        for(int i = 0; i < this.numeroImagens;i++){ 
            
            while(true){
                x = gerador.nextInt(36);        
                if(!arrlist.contains(x)){
                    arrlist.add(x);
                    break;
                }
            }
            
            this.rodada[i][0] = this.getX(x);
            this.rodada[i][1] = this.getY(x);

            g.drawImage(imgs[0], this.getX(x),this.getY(x), null);
        }
        
        int q = 0;
        

        try {
            
            Thread.sleep(3000);
        } catch (InterruptedException ex) {
        
        }
       
       g.clearRect(0, 0, 800, 1000);
    }
    
     public int getX(int i){
       
        return this.posicao[i][0];
    }
    
    public int getY(int i){
       
        return this.posicao[i][1];
    }

    private int getAux() {
        return aux;
    }
    
}

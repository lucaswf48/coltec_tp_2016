/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication26;



import javax.swing.JOptionPane;


public class ConfereClick implements Runnable{
    
    JavaApplication26 g;
    
    public ConfereClick(JavaApplication26 g){
        this.g = g;
    }
    
    @Override
    public void run() {
        while(true){
            if(System.currentTimeMillis() - g.getClick() > 8000 && g.isTerminouDeMostrar()){
                
                JOptionPane.showMessageDialog(g, "Demorou mais de 8 segundos para clicar!");
                g.principal();
            }
            try {
                Thread.sleep(500);
                
            } catch (InterruptedException ex) {
            }
        }
    }
    
}
